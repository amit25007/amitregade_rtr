#include<stdio.h>

//declaration
int multiplication();
int sum();
int updateVariable(int);

//Global variables
int c, z;

//Constant
const int constant1 = 100;

//global static variables
int static x, y;

void main()
{
	//local variables
	int a=5, b=10;
	x = 10;
	y = 20;
	float f=0.5;

	//local static variable
	int s1 = 0;

	c = a + b;
	printf("Value of c is %d\n", c);

	z=multiplication();
	printf("Value of x*y is %d\n", z);
	x = x + 2;
	y = y - 5;
	z = multiplication();
	printf("new Value of x*y is %d\n", z);
	
	int s;
	s = sum();
	printf("Sum of c and z is :%d\n", s);

	//change static var
	updateVariable(s1);
	printf("Current value of s1 is :%d\n", s1);

	//format string, 	//escape sequence
	printf("This is single line\n");
	printf("This is\n multiline\n statement\n");
	printf("Current directory is : F:\\RTR\\Operators\\");

	//Constants
	updateVariable(constant1);
	printf("Value of constant: %d\n", constant1);

	//operators
	int f1, d, e;
	float g, h;
	//arithmetic
	d = a + b;
	e = a - b;
	f1 = a * b;
	g = a / b;
	h = a % b;
	a = b;

	printf("Arithmetic: Sum of a and b is :%d\n", d);
	printf("Arithmetic: Subtraction of a and b is :%d\n", e);
	printf("Arithmetic: Multiplication of a and b is :%d\n", f1);
	printf("Arithmetic: Division of a and b is :%f\n", g);
	printf("Arithmetic: Modulus of a and b is :%f\n", h);
	printf("Assignment: b is assigned to a b:%d hence a:%d\n", b, a);


	//compound arithmetic operators
	d = (a + b)*c;
	printf("Compund value of d is: %d\n", d);

	//increament/ decreament
	printf("increament: Value of a is %d\n", a);
	a = a + 1;
	printf("increament: Value of a after increament is %d\n", a);
	printf("decreament: Value of b is %d\n", b);
	b = b - 1;
	printf("decreament: Value of b after decreament is %d\n", b);

	//Relational 
	if (a > b)
		printf("a is greater than b\n");
	else if (a == b)
		printf("a is equal to b");
	else
		printf("b is greater than a\n");

	//relational
	updateVariable(a);
	if (a > b)
	{
		printf("a is greater than b");
	}

	//logical
	a = 5;
	b = 10;
	c = 20;
	d = 25;

	if (((a < b)&&(b<c))||c>d)
	{
		printf("condition is true");
	}

	//bitwise operators
	a = 20, b = 60;
	printf("a&b = %d\n", a&b); 
	printf("a|b = %d\n", a | b); 
	printf("a^b = %d\n", a^b);
	printf("~a = %d\n", a = ~a);
	printf("b<<1 = %d\n", b << 1);
	printf("b>>1 = %d\n", b >> 1);
	
	//ternery 
	int largesNumber, p, q, r;
	printf("Enter 3 numbers: ");
	scanf_s("%d%d%d", &p, &q, &r);
	largesNumber = p > q ? (p > r ? p : r) : (q > r ? q : r);
	printf("Largest Number is: %d", largesNumber);

	//Implicit type conversion
	int a1 = 100;
	float b1 = 12.5;
	float c1 = a + b;
	printf("\nImplicite: Value of c1 is %f", c1);

	//explicite
	int a2 = 50, b2 = 6;
	float c2;
	c2 = (float)(a2 / b2);
	printf("\nExplicite: Value of c2 is %f", c2);
}


int multiplication() {
	int returnValue;
	returnValue = x * y;
	return(returnValue);
}

int sum()
{
	int sumValue = c + z;
	return(sumValue);
}

int updateVariable(int arg)
{
	arg = arg + 15;
	return(arg);
}

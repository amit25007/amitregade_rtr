#include <stdio.h>
#include <conio.h>

int main(){
	int  x, y, c;
	int NumbersArray[50], number = 0;
	x = 0;
	y=0;
	c = 0;
	printf("\nEnter the number of elements to sort : ");
	scanf("%d", &number);

	for (x = 0; x < number; x++)
	{
		printf("\nEnter number %d : ", x+1);
		scanf("%d", &NumbersArray[x]);
	}
		
	printf("\n\nEntered numbers are :\n\n");

	for (x = 0; x < number; x++)
		printf("%d\n", NumbersArray[x]);

	for (x = 0; x < number - 1; x++)
	{
		for (y = 0; y < number - x - 1; y++)
		{
			if (NumbersArray[y] > NumbersArray[y + 1])
			{
				c = NumbersArray[y];
				NumbersArray[y] = NumbersArray[y + 1];
				NumbersArray[y + 1] = c;
			}
		}
	}

	printf("\n\nSorted list in ascending order :\n\n");

	for (x = 0; x < number; x++)
		printf("%d\n", NumbersArray[x]);

	_getch();
	return 0;
}
#include <stdio.h>
int main(void)
{
	//variable declarations
	int numberForIfCondition;

	//code
	printf("\n\n");

	numberForIfCondition = 5;
	if (numberForIfCondition) // Non-zero Positive Value
	{
		printf("if-block 1 : 'NumberForIfCondition' Exists And Has Value = %d !!!\n\n", numberForIfCondition);
	}

	numberForIfCondition = -5;
	if (numberForIfCondition) // Non-zero Negative Value
	{
		printf("if-block 2 : 'NumberForIfCondition' Exists And Has Value = %d !!!\n\n", numberForIfCondition);
	}

	numberForIfCondition = 0;
	if (numberForIfCondition) // Zero Value
	{
		printf("if-block 3 : 'NumberForIfCondition' Exists And Has Value = %d !!!\n\n", numberForIfCondition);
	}

	printf("All Three if-statements Are Done !!!\n\n");

	_getch();
	return(0);
}

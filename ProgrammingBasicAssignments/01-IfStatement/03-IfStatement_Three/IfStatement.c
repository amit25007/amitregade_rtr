#include <stdio.h>
int main(void)
{
	//variable declarations
	int number;

	//code
	printf("\n\n");

	printf("Enter Value For 'number' : ");
	scanf("%d", &number);

	if (number < 0)
	{
		printf("number = %d Is Less Than 0 (NEGATIVE).\n\n", number);
	}

	if ((number > 0) && (number <= 100))
	{
		printf("number = %d Is Between 0 And 100.\n\n", number);
	}

	if ((number > 100) && (number <= 200))
	{
		printf("number = %d Is Between 100 And 200.\n\n", number);
	}

	if ((number > 200) && (number <= 300))
	{
		printf("number = %d Is Between 200 And 300.\n\n", number);
	}

	if ((number > 300) && (number <= 400))
	{
		printf("number = %d Is Between 300 And 400.\n\n", number);
	}

	if ((number > 400) && (number <= 500))
	{
		printf("number = %d Is Between 400 And 500.\n\n", number);
	}

	if (number > 500)
	{
		printf("number = %d Is Greater Than 500.\n\n", number);
	}

	_getch();
	return(0);
}

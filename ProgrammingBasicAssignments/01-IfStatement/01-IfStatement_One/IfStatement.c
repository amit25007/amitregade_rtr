#include <stdio.h>
int main(void)
{
	//variable declarations
	int firstNumber, secondNumber, thirdNumber;

	//code
	firstNumber = 9;
	secondNumber = 30;
	thirdNumber = 30;

	printf("\n\n");

	if (firstNumber < secondNumber)
	{
		printf("First Number %d Is Less Than Second Number %d !!!\n\n", firstNumber, secondNumber);
	}

	if (secondNumber != thirdNumber)
	{
		printf("Second Number %d Is NOT Equal To Third Number %d !!!\n\n", secondNumber, thirdNumber);
	}

	printf("Both Comparisons Have Been Done !!!\n\n");
	getchar();
	return(0);
}

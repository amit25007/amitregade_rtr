#include <stdio.h>
int main(void)
{
	//variable declarations
	int num1, num2;

	//code
	printf("\n\n");

	printf("Outer Loop Prints Odd Numbers Between 1 and 10. \n\n");
	printf("Inner Loop Prints Even Numbers Between 1 and 10 For Every Odd Number Printed By Outer Loop. \n\n");

	// condition for a number to be even number => division of number by 2 leaves no remainder (remainder = 0)
	// condition for a number to be odd number => division of number by 2 leaves remainder (remainder = 1 (usually))

	for (num1 = 1; num1 <= 10; num1++)
	{
		if (num1 % 2 != 0) //If Number (i) Is Odd..
		{
			printf("i = %d\n", num1);
			printf("---------\n");
			for (num2 = 1; num2 <= 10; num2++)
			{
				if (num2 % 2 == 0) //If Number (j) Is Even...
				{
					printf("\tj = %d\n", num2);
				}
				else //If Number (j) Is Odd..
				{
					continue;
				}
			}
			printf("\n\n");
		}
		else //If Number (i) Is Even...
		{
			continue;
		}
	}

	printf("\n\n");

	_getch();
	return(0);
}

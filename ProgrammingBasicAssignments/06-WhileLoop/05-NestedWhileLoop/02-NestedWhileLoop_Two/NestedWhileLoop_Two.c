#include <stdio.h>
int main(void)
{
	//variable declarations
	int number1, number2, number3;

	//code
	printf("\n\n");
	
	number1 = 1;
	while (number1 <= 10)
	{
		printf("Number 1 = %d\n", number1);
		printf("--------\n\n");
		
		number2 = 1;
		while (number2 <= 5)
		{
			printf("\tNumber 2 = %d\n", number2);
			printf("\t--------\n\n");
			
			number3 = 1;
			while (number3 <= 3)
			{
				printf("\t\tk = %d\n", number3);
				number3++;
			}
			printf("\n\n");
			number2++;
		}
		printf("\n\n");
		number1++;
	}

	_getch();
	return(0);
}

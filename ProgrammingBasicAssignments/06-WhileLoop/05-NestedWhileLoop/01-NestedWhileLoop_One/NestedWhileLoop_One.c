#include <stdio.h>
int main(void)
{
	//variable declarations
	int num1, num2;

	//code
	printf("\n\n");

	num1 = 1;
	while(num1 <= 10)
	{
		printf("Num1 = %d\n", num1);
		printf("--------\n\n");
		
		num2 = 1;
		while (num2 <= 5)
		{
			printf("\tj = %d\n", num2);
			num2++;
		}
		num1++;
		printf("\n\n");
	}

	_getch();
	return(0);
}

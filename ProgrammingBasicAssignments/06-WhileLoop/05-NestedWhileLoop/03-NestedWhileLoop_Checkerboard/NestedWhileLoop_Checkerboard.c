#include <stdio.h>
int main(void)
{
	//variable declarations
	int i, j, check;

	//code
	printf("\n\n");

	i = 0;
	while (i < 64)
	{
		j = 0;
		while (j < 64)
		{
			check = ((i & 0x8) == 0) ^ ((j & 0x8) == 0);

			if (check == 0)
				printf("  ");

			if (check == 1)
				printf("* ");

			j++;

		}
		printf("\n\n");
		i++;
	}

	_getch();
	return(0);
}

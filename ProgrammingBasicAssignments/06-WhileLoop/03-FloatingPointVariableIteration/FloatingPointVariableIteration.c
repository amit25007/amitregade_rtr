#include <stdio.h>
int main(void)
{
	//variable declarations
	float flotingPointNumber;
	float incrementingFloatNumber = 1.7f; //simply change this value ONLY to get different outputs...

	//code
	printf("\n\n");

	printf("Printing Numbers %f to %f : \n\n", incrementingFloatNumber, (incrementingFloatNumber * 10.0f));

	flotingPointNumber = incrementingFloatNumber;
	while(flotingPointNumber <= (incrementingFloatNumber * 10.0f))
	{
		printf("\t%f\n", flotingPointNumber);
		flotingPointNumber = flotingPointNumber + incrementingFloatNumber;
	}

	printf("\n\n");

	_getch();
	return(0);
}

#include <stdio.h>
int main(void)
{
	//variable declarations
	int initialNumber, number, i;

	//code
	printf("\n\n");

	printf("Enter An Integer Value From Which Iteration Must Begin : ");
	scanf("%d", &initialNumber);

	printf("How Many Digits Do You Want To Print From %d Onwards ? : ", initialNumber);
	scanf("%d", &number);

	printf("Printing Digits %d to %d : \n\n", initialNumber, (initialNumber + number));

	i = initialNumber;
	while (i <= (initialNumber + number))
	{
		printf("\t%d\n", i);
		i++;
	}

	printf("\n\n");

	_getch();
	return(0);
}

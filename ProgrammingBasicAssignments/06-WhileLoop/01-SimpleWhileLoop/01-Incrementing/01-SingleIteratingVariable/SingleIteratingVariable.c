#include <stdio.h>
int main(void)
{
	//variable declarations
	int number;

	//code
	printf("\n\n");

	printf("Printing Digits 1 to 10 : \n\n");

	number = 1;
	while (number <= 10)
	{
		printf("\t%d\n", number);
		number++;
	}

	printf("\n\n");

	_getch();
	return(0);
}

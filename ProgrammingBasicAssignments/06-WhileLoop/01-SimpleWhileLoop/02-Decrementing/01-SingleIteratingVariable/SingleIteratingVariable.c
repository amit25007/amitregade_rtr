#include <stdio.h>
int main(void)
{
	//variable declarations
	int number;

	//code
	printf("\n\n");

	printf("Printing Digits 10 to 1 : \n\n");

	number = 10;
	while (number >= 1)
	{
		printf("\t%d\n", number);
		number--;
	}

	printf("\n\n");

	_getch();
	return(0);
}

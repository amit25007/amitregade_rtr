#include <stdio.h>
int main(void)
{
	//variable declarations
	int initialNumber, endingNumber, incrementingNumber;

	//code
	printf("\n\n");

	printf("Enter An Integer Value From Which Iteration Must Begin : ");
	scanf("%d", &initialNumber);

	printf("How Many Digits Do You Want To Print From %d Onwards ? : ", initialNumber);
	scanf("%d", &endingNumber);

	printf("Printing Digits %d to %d : \n\n", initialNumber, (initialNumber + endingNumber));

	for (incrementingNumber = initialNumber; incrementingNumber <= (initialNumber + endingNumber); incrementingNumber++)
	{
		printf("\t%d\n", incrementingNumber);
	}

	printf("\n\n");

	_getch();
	return(0);
}

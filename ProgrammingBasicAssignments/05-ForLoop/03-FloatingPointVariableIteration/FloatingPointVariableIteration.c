#include <stdio.h>
int main(void)
{
	//variable declarations
	float floatingPointNumber;
	float floatIncrementNumber = 1.7f; //simply change this value ONLY to get different outputs...

	//code
	printf("\n\n");

	printf("Printing Numbers %f to %f : \n\n", floatIncrementNumber, (floatIncrementNumber * 10.0f));

	for (floatingPointNumber = floatIncrementNumber; floatingPointNumber <= (floatIncrementNumber * 10.0f); floatingPointNumber = floatingPointNumber + floatIncrementNumber)
	{
		printf("\t%f\n", floatingPointNumber);
	}

	printf("\n\n");

	_getch();
	return(0);
}

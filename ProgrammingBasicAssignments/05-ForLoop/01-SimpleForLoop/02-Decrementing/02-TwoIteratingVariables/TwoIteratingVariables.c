#include <stdio.h>
int main(void)
{
	//variable declarations
	int decrementByOne, decrementByTen;

	//code
	printf("\n\n");

	printf("Printing Digits 10 to 1 and 100 to 10 : \n\n");

	for (decrementByOne = 10, decrementByTen = 100; decrementByOne >= 1, decrementByTen >= 10; decrementByOne--, decrementByTen -= 10)
	{
		printf("\t %d \t %d\n", decrementByOne, decrementByTen);
	}

	printf("\n\n");

	_getch();
	return(0);
}

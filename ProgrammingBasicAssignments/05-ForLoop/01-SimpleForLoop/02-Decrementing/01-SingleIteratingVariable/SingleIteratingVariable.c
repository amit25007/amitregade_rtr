#include <stdio.h>
int main(void)
{
	//variable declarations
	int decrementByOne;

	//code
	printf("\n\n");

	printf("Printing Digits 10 to 1 : \n\n");

	for (decrementByOne = 10; decrementByOne >= 1; decrementByOne--)
	{
		printf("\t%d\n", decrementByOne);
	}

	printf("\n\n");

	_getch();
	return(0);
}

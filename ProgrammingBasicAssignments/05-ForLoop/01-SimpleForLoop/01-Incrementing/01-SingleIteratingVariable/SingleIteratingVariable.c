#include <stdio.h>
int main(void)
{
	//variable declarations
	int incrementNumberByOne;

	//code
	printf("\n\n");

	printf("Printing Digits 1 to 10 : \n\n");

	for (incrementNumberByOne = 1; incrementNumberByOne <= 10; incrementNumberByOne++)
	{
		printf("\t%d\n", incrementNumberByOne);
	}

	printf("\n\n");

	_getch();
	return(0);
}

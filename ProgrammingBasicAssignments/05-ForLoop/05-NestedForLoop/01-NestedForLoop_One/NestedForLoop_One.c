#include <stdio.h>
int main(void)
{
	//variable declarations
	int firstNumber, secondNumber;

	//code
	printf("\n\n");
	for (firstNumber = 1; firstNumber <= 10; firstNumber++)
	{
		printf("FirstNumber = %d\n", firstNumber);
		printf("--------\n\n");
		for (secondNumber = 1; secondNumber <= 5; secondNumber++)
		{
			printf("\tSecondNumber = %d\n", secondNumber);
		}
		printf("\n\n");
	}

	_getch();
	return(0);
}

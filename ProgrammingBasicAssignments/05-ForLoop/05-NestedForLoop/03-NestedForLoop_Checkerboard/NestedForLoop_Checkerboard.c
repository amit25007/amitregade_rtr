#include <stdio.h>
int main(void)
{
	//variable declarations
	int i, j, checkVariable;

	//code
	printf("\n\n");
	for (i = 0; i < 64; i++)
	{
		for (j = 0; j < 64; j++)
		{
			checkVariable = ((i & 0x8) == 0) ^ ((j & 0x8) == 0);

			if (checkVariable == 0)
				printf("  ");

			if (checkVariable == 1)
				printf("* ");				

		}
		printf("\n\n");
	}

	_getch();
	return(0);
}

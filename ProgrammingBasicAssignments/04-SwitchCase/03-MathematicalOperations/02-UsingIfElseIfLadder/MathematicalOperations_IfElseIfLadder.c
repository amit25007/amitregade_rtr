#include <stdio.h> // for printf()
#include <conio.h> // for getch()

int main(void)
{
	//variable declarations
	int number1, number2;
	int result;

	char mathsOperationChoice, divisionChoice;

	//code
	printf("\n\n");

	printf("Enter Value For 'Number 1' : ");
	scanf("%d", &number1);

	printf("Enter Value For 'Number 2' : ");
	scanf("%d", &number2);

	printf("Enter mathsOperationChoice In Character : \n\n");
	printf("'A' or 'a' For Addition : \n");
	printf("'S' or 's' For Subtraction : \n");
	printf("'M' or 'm' For Multiplication : \n");
	printf("'D' or 'd' For Division : \n\n");

	printf("Enter mathsOperationChoice : ");
	mathsOperationChoice = getch();

	printf("\n\n");

	if (mathsOperationChoice == 'A' || mathsOperationChoice == 'a')
	{
		result = number1 + number2;
		printf("Addition Of A = %d And B = %d Gives Result %d !!!\n\n", number1, number2, result);
	}

	else if (mathsOperationChoice == 'S' || mathsOperationChoice == 's')
	{
		if (number1 >= number2)
		{
			result = number1 - number2;
			printf("Subtraction Of B = %d From A = %d Gives Result %d !!!\n\n", number2, number1, result);
		}
		else
		{
			result = number2 - number1;
			printf("Subtraction Of A = %d From B = %d Gives Result %d !!!\n\n", number1, number2, result);
		}
	}

	else if(mathsOperationChoice == 'M' || mathsOperationChoice == 'm')
	{
		result = number1 * number2;
		printf("Multiplication Of A = %d And B = %d Gives Result %d !!!\n\n", number1, number2, result);
	}
	
	else if (mathsOperationChoice == 'D' || mathsOperationChoice == 'd')
	{
		printf("Enter mathsOperationChoice In Character : \n\n");
		printf("'Q' or 'q' or '/' For Quotient Upon Division : \n");
		printf("'R' or 'r' or '%%' For Remainder Upon Division : \n");

		printf("Enter mathsOperationChoice : ");
		divisionChoice = getch();

		printf("\n\n");
		
		if (divisionChoice == 'Q' || divisionChoice == 'q' || divisionChoice == '/')
		{
			if (number1 >= number2)
			{
				result = number1 / number2;
				printf("Division Of A = %d By B = %d Gives Quotient = %d !!!\n\n", number1, number2, result);
			}
			else
			{
				result = number2 / number1;
				printf("Division Of B = %d By A = %d Gives Quotient = %d !!!\n\n", number2, number1, result);
			}
		}

		else if (divisionChoice == 'R' || divisionChoice == 'r' || divisionChoice == '%')
		{
			if (number1 >= number2)
			{
				result = number1 % number2;
				printf("Division Of A = %d By B = %d Gives Remainder = %d !!!\n\n", number1, number2, result);
			}
			else
			{
				result = number2 / number1;
				printf("Division Of B = %d By A = %d Gives Remainder = %d !!!\n\n", number2, number1, result);
			}
		}
		
		else
			printf("Invalid Character %c Entered For Division !!! Please Try Again...\n\n", divisionChoice);
	}

	else
		printf("Invalid Character %c Entered !!! Please Try Again...\n\n", mathsOperationChoice);

	printf("If - Else If - Else Ladder Complete !!!\n");

	_getch();
	return(0);
}

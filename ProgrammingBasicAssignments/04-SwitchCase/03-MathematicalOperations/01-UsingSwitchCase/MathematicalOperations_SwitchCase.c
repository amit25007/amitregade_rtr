#include <stdio.h> // for printf() 
#include <conio.h> //for getch()

int main(void)
{
	//variable declarations
	int num1, num2;
	int result;

	char choice, divisionChoice;

	//code
	printf("\n\n");

	printf("Enter Value For 'Num1' : ");
	scanf("%d", &num1);

	printf("Enter Value For 'Num2' : ");
	scanf("%d", &num2);

	printf("Enter choice In Character : \n\n");
	printf("'A' or 'a' For Addition : \n");
	printf("'S' or 's' For Subtraction : \n");
	printf("'M' or 'm' For Multiplication : \n");
	printf("'D' or 'd' For Division : \n\n");

	printf("Enter choice : ");
	choice = getch();

	printf("\n\n");

	switch (choice)
	{
	// FALL THROUGH CONSITION FOR 'A' and 'a'
	case 'A':
	case 'a':
		result = num1 + num2;
		printf("Addition Of Num1 = %d And Num2 = %d Gives Result %d !!!\n\n", num1, num2, result);
		break;

	// FALL THROUGH CONSITION FOR 'S' and 's'
	case 'S':
	case 's':
		if (num1 >= num2)
		{
			result = num1 - num2;
			printf("Subtraction Of Num2 = %d From Num1 = %d Gives Result %d !!!\n\n", num2, num1, result);
		}
		else
		{
			result = num2 - num1;
			printf("Subtraction Of Num1 = %d From Num2 = %d Gives Result %d !!!\n\n", num1, num2, result);
		}
		break;

	// FALL THROUGH CONSITION FOR 'M' and 'm'
	case 'M':
	case 'm':
		result = num1 * num2;
		printf("Multiplication Of Num1 = %d And Num2 = %d Gives Result %d !!!\n\n", num1, num2, result);
		break;

	// FALL THROUGH CONSITION FOR 'D' and 'd'
	case 'D':
	case 'd':
		printf("Enter Option In Character : \n\n");
		printf("'Q' or 'q' or '/' For Quotient Upon Division : \n");
		printf("'R' or 'r' or '%%' For Remainder Upon Division : \n");

		printf("Enter Option : ");
		divisionChoice = getch();

		printf("\n\n");

		switch (divisionChoice)
		{
		// FALL THROUGH CONSITION FOR 'Q' and 'q' and '/'
		case 'Q':
		case 'q':
		case '/':
			if (num1 >= num2)
			{
				result = num1 / num2;
				printf("Division Of Num1 = %d By Num2 = %d Gives Quotient = %d !!!\n\n", num1, num2, result);
			}
			else
			{
				result = num2 / num1;
				printf("Division Of Num2 = %d By Num1 = %d Gives Quotient = %d !!!\n\n", num2, num1, result);
			}
			break; // 'break' of case 'Q' or case 'q' or case '/'

	   // FALL THROUGH CONSITION FOR 'R' and 'r' and '%'
		case 'R':
		case 'r':
		case '%':
			if (num1 >=num2)
			{
				result = num1 % num2;
				printf("Division Of Num1 = %d By Num2 = %d Gives Remainder = %d !!!\n\n", num1, num2, result);
			}
			else
			{
				result = num2 / num1;
				printf("Division Of Num2 = %d By Num1 = %d Gives Remainder = %d !!!\n\n", num2, num1, result);
			}
			break; // 'break' of case 'R' or case 'r' or case '%'

		default: // 'default' case for switch(divisionChoice)
			printf("Invalid Character %c Entered For Division !!! Please Try Again...\n\n", divisionChoice);
			break; // 'break' of 'default' of switch(divisionChoice)

		} // ending curly brace of switch(divisionChoice)

		break; // 'break' of case 'D' or case 'd'

	default: // 'default' case for switch (option)
		printf("Invalid Character %c Entered !!! Please Try Again...\n\n", choice);
		break;
	} // ending curly brace of switch(option)

	printf("Switch Case Block Complete !!!\n");

	_getch();
	return(0);
}

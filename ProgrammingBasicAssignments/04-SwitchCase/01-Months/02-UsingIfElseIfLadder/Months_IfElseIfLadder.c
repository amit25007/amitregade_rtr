#include <stdio.h>
int main(void)
{
	//variable declarations
	int monthNumber;

	//code
	printf("\n\n");

	printf("Enter Number Of Month (1 to 12) : ");
	scanf("%d", &monthNumber);

	printf("\n\n");

	// IF - ELSE - IF LADDER BEGINS FROM HERE...
	if (monthNumber == 1) //like 'case 1'
		printf("Month Number %d Is JANUARY !!!\n\n", monthNumber);

	else if (monthNumber == 2) //like 'case 2'
		printf("Month Number %d Is FEBRUARY !!!\n\n", monthNumber);

	else if (monthNumber == 3) //like 'case 3'
		printf("Month Number %d Is MARCH !!!\n\n", monthNumber);

	else if (monthNumber == 4) //like 'case 4'
		printf("Month Number %d Is APRIL !!!\n\n", monthNumber);

	else if (monthNumber == 5) //like 'case 5'
		printf("Month Number %d Is MAY !!!\n\n", monthNumber);

	else if (monthNumber == 6) //like 'case 6'
		printf("Month Number %d Is JUNE !!!\n\n", monthNumber);

	else if (monthNumber == 7) //like 'case 7'
		printf("Month Number %d Is JULY !!!\n\n", monthNumber);

	else if (monthNumber == 8) //like 'case 8'
		printf("Month Number %d Is AUGUST !!!\n\n", monthNumber);

	else if (monthNumber == 9) //like 'case 9'
		printf("Month Number %d Is SEPTEMBER !!!\n\n", monthNumber);

	else if (monthNumber == 10) //like 'case 10'
		printf("Month Number %d Is OCTOBER !!!\n\n", monthNumber);

	else if (monthNumber == 11) //like 'case 11'
		printf("Month Number %d Is NOVEMBER !!!\n\n", monthNumber);

	else if (monthNumber == 12) //like 'case 12'
		printf("Month Number %d Is DECEMBER !!!\n\n", monthNumber);

	else //like 'default'...just like 'default' is optional in switch-case, so is 'else' in the if-else if-else ladder...
		printf("Invalid Month Number %d Entered !!! Please Try Again...\n\n", monthNumber);

	printf("If - Else If - Else Ladder Complete !!!\n");

	_getch();
	return(0);
}

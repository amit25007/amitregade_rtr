#include <stdio.h>
int main(void)
{
	//variable declarations
	int monthNumber;

	//code
	printf("\n\n");

	printf("Enter Number Of Month (1 to 12) : ");
	scanf("%d", &monthNumber);

	printf("\n\n");

	switch (monthNumber)
	{
	case 1: //like 'if'
		printf("Month Number %d Is JANUARY !!!\n\n", monthNumber);
		break;

	case 2: //like 'else if'
		printf("Month Number %d Is FEBRUARY !!!\n\n", monthNumber);
		break;

	case 3: //like 'else if'
		printf("Month Number %d Is MARCH !!!\n\n", monthNumber);
		break;

	case 4: //like 'else if'
		printf("Month Number %d Is APRIL !!!\n\n", monthNumber);
		break;

	case 5: //like 'else if'
		printf("Month Number %d Is MAY !!!\n\n", monthNumber);
		break;

	case 6: //like 'else if'
		printf("Month Number %d Is JUNE !!!\n\n", monthNumber);
		break;

	case 7: //like 'else if'
		printf("Month Number %d Is JULY !!!\n\n", monthNumber);
		break;

	case 8: //like 'else if'
		printf("Month Number %d Is AUGUST !!!\n\n", monthNumber);
		break;

	case 9: //like 'else if'
		printf("Month Number %d Is SEPTEMBER !!!\n\n", monthNumber);
		break;

	case 10: //like 'else if'
		printf("Month Number %d Is OCTOBER !!!\n\n", monthNumber);
		break;

	case 11: //like 'else if'
		printf("Month Number %d Is NOVEMBER !!!\n\n", monthNumber);
		break;

	case 12: //like 'else if'
		printf("Month Number %d Is DECEMBER !!!\n\n", monthNumber);
		break;

	default: //like ending OPTIONAL 'else'...just like terminating 'else' is optional in if-else if-else ladder, so is the 'default' case optional in switch-case
		printf("Invalid Month Number %d Entered !!! Please Try Again...\n\n", monthNumber);
		break;
	}

	printf("Switch Case Block Complete !!!\n");

	_getch();
	return(0);
}

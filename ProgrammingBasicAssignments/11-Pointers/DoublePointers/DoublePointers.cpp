#include <stdio.h> 
#include <stdlib.h> 

int main()
{
	int row = 5, col = 3;
	int a[5][3];

	int *ptr[5];

	int i, j, count = 0;
	for (i = 0; i < row; i++) 
	{
		ptr[i] = (int *)malloc(row * sizeof(int));
		if (ptr[i]==0)
		{
			exit(0);
		}
		for (j = 0; j < col; j++)
		{
			a[i][j] = (i+1) * (j+1);
			ptr[i] = &a[i][j];
		}
	}
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			printf("Value of a[%d][%d] is %d\t", i, j, a[i][j]);
			printf("address of a[%d] is %x\n", i, ptr[i]);
		}
	}

	return 0;
}

#include <stdio.h>
#include <conio.h>
int main(void)
{
	//variable declarations
	int num1, num2;

	//code
	printf("\n\n");

	for (num1 = 1; num1 <= 20; num1++)
	{
		for (num2 = 1; num2 <= 20; num2++)
		{
			if (num2 > num1)
			{
				break;
			}
			else
			{
				printf("* ");
			}
		}
		printf("\n");
	}
	printf("\n\n");

	_getch();
	return(0);
}

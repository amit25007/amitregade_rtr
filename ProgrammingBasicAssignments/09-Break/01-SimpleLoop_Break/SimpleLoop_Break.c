#include <stdio.h>
#include <conio.h>
int main(void)
{
	//variable declarations
	int number;
	char ch;

	//code
	printf("\n\n");

	printf("Printing Even Numbers From 1 to 100 For Every User Input. Exitting the Loop When User Enters Character 'Q' or 'q' : \n\n");
	printf("Enter Character 'Q' or 'q' To Exit Loop : \n\n");

	for (number = 1; number <= 100; number++)
	{
		printf("\t%d\n", number);
		ch = getch();
		if (ch == 'Q' || ch == 'q')
		{
			break;
		}
	}

	printf("\n\n");
	printf("EXITTING LOOP...");
	printf("\n\n");

	_getch();
	return(0);
}

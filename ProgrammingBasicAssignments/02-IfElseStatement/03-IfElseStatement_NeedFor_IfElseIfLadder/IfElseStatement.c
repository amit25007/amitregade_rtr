#include <stdio.h>
int main(void)
{
	//variable declarations
	int number;

	//code
	printf("\n\n");

	printf("Enter Value For 'number' : ");
	scanf("%d", &number);

	if (number < 0) // 'if' - 01
	{
		printf("number = %d Is Less Than 0 (NEGATIVE) !!!\n\n", number);
	}
	else // 'else' - 01
	{
		if ((number > 0) && (number <= 100)) // 'if' - 02
		{
			printf("number = %d Is Between 0 And 100 !!!\n\n", number);
		}
		else // 'else' - 02
		{
			if ((number > 100) && (number <= 200)) // 'if' - 03
			{
				printf("number = %d Is Between 100 And 200 !!!\n\n", number);
			}
			else // 'else' - 03
			{
				if ((number > 200) && (number <= 300)) // 'if' - 04
				{
					printf("number = %d Is Between 200 And 300 !!!\n\n", number);
				}
				else // 'else' - 04
				{
					if ((number > 300) && (number <= 400)) // 'if' - 05
					{
						printf("number = %d Is Between 300 And 400 !!!\n\n", number);
					}
					else // 'else' - 05
					{
						if ((number > 400) && (number <= 500)) // 'if' - 06
						{
							printf("number = %d Is Between 400 And 500 !!!\n\n", number);
						}
						else // 'else' - 06
						{
							printf("number = %d Is Greater Than 500 !!!\n\n", number);
						} // closing brace of 'else' - 06

					} // closing brace of 'else' - 05

				} // closing brace of 'else' - 04

			} // closing brace of 'else' - 03

		} // closing brace of 'else' - 02

	} // closing brace of 'else' - 01

	_getch();
	return(0);
}

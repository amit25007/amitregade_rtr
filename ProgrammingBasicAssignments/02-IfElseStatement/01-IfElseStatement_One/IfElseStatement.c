#include <stdio.h>
int main(void)
{
	//variable declarations
	int number1, number2, number3;

	//code
	number1 = 9;
	number2 = 30;
	number3 = 30;

	// *** FIRST if-else PAIR ***
	printf("\n\n");
	if (number1 < number2)
	{
		printf("Entering First if-block...\n\n");
		printf("Number1 %d Is Less Than Number2 %d !!!\n\n", number1, number2);
	}
	else
	{
		printf("Entering First else-block...\n\n");
		printf("Number1 %d Is NOT Less Than Number2 %d !!!\n\n", number1, number2);
	}
	printf("First if-else Pair Done !!!\n\n");

	// *** SECOND if-else PAIR ***
	printf("\n\n");
	if (number2 != number3)
	{
		printf("Entering Second if-block...\n\n");
		printf("Number2 %d Is NOT Equal To Number3 %d !!!\n\n", number2, number3);
	}
	else
	{
		printf("Entering Second else-block...\n\n");
		printf("Number2 %d Is Equal To Number3 %d !!!\n\n", number2, number3);
	}
	printf("Second if-else Pair Done !!!\n\n");
	
	_getch();
	return(0);
}

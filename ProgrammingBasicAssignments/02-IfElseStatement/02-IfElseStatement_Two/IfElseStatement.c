#include <stdio.h>
int main(void)
{
	//variable declarations
	int ageCheckForVoting;

	//code
	printf("\n\n");
	printf("Enter Age : ");
	scanf("%d", &ageCheckForVoting);
	printf("\n\n");
	if (ageCheckForVoting >= 18)
	{
		printf("Entering if-block...\n\n");
		printf("You Are Eligible For Voting !!!\n\n");
	}
	else
	{
		printf("Entering else-block...\n\n");
		printf("You Are NOT Eligible For Voting !!!\n\n");
	}
	printf("Bye !!!\n\n");

	_getch();
	return(0);
}

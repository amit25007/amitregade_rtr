#include <stdio.h>
int main(void)
{
	//variable declarations
	int numberCheck;

	//code
	printf("\n\n");

	printf("Enter Value For 'numberCheck' : ");
	scanf("%d", &numberCheck);

	// IF - ELSE - IF LADDER BEGINS FROM HERE...
	if (numberCheck < 0)
		printf("Num = %d Is Less Than 0 (NEGATIVE) !!!\n\n", numberCheck);

	else if ((numberCheck > 0) && (numberCheck <= 100))
		printf("Num = %d Is Between 0 And 100 !!!\n\n", numberCheck);

	else if ((numberCheck > 100) && (numberCheck <= 200))
		printf("Num = %d Is Between 100 And 200 !!!\n\n", numberCheck);

	else if ((numberCheck > 200) && (numberCheck <= 300)) 
		printf("Num = %d Is Between 200 And 300 !!!\n\n", numberCheck);

	else if ((numberCheck > 300) && (numberCheck <= 400)) 
		printf("Num = %d Is Between 300 And 400 !!!\n\n", numberCheck);

	else if ((numberCheck > 400) && (numberCheck <= 500)) 
		printf("Num = %d Is Between 400 And 500 !!!\n\n", numberCheck);

	else if (numberCheck > 500)
		printf("Num = %d Is Greater Than 500 !!!\n\n", numberCheck);

	// *** NO TERMINATING 'ELSE' IN THIS LADDER !!! ***
	_getch();
	return(0);
}

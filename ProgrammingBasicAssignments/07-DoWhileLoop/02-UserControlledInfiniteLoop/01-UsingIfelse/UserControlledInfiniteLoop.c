#include <stdio.h>
int main(void)
{
	//variable declarations
	char choice, character = '\0';

	//code
	printf("\n\n");
	printf("Once The Infinite Loop Begins, Enter 'Q' or 'q' To Quit The Infinite For Loop : \n\n");
	printf("Enter 'Y' oy 'y' To Initiate User Controlled Infinite Loop : ");
	printf("\n\n");
	choice = getch();
	if (choice == 'Y' || choice == 'y')
	{
		do
		{
			printf("In Loop...\n");
			character = getch(); //control flow waits for character input...
			if (character == 'Q' || character == 'q')
				break; //User Controlled Exitting From Infinite Loop
		} while (1); //Infinite Loop

		printf("\n\n");
		printf("EXITTING USER CONTROLLED INFINITE LOOP...");
		printf("\n\n");
	}

	else
		printf("You Must Press 'Y' or 'y' To Initiate The User Controlled Infinite Loop....Please Try Again...\n\n");

	_getch();
	return(0);
}

#include <stdio.h>
int main(void)
{
	//variable declarations
	int incrementByOne, incrementByTen;

	//code
	printf("\n\n");

	printf("Printing Digits 1 to 10 and 10 to 100: \n\n");

	incrementByOne = 1;
	incrementByTen = 10;
	do
	{
		printf("\t %d \t %d\n", incrementByOne, incrementByTen);
		incrementByOne++;
		incrementByTen = incrementByTen + 10;
	}while (incrementByOne <= 10, incrementByTen <= 100);

	printf("\n\n");

	_getch();
	return(0);
}

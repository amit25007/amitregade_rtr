#include <stdio.h>
int main(void)
{
	//variable declarations
	int number;

	//code
	printf("\n\n");

	printf("Printing Digits 1 to 10 : \n\n");

	number = 1;
	do
	{
		printf("\t%d\n", number);
		number++;
	}while (number <= 10);

	printf("\n\n");

	_getch();
	return(0);
}

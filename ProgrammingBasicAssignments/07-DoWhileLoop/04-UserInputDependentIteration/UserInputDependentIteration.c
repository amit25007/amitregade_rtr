#include <stdio.h>
int main(void)
{
	//variable declarations
	int initialNumber, count, i;

	//code
	printf("\n\n");

	printf("Enter An Integer Value From Which Iteration Must Begin : ");
	scanf("%d", &initialNumber);

	printf("How Many Digits Do You Want To Print From %d Onwards ? : ", initialNumber);
	scanf("%d", &count);

	printf("Printing Digits %d to %d : \n\n", initialNumber, (initialNumber + count));

	i = initialNumber;
	do
	{
		printf("\t%d\n", i);
		i++;
	}while (i <= (initialNumber + count));

	printf("\n\n");

	_getch();
	return(0);
}

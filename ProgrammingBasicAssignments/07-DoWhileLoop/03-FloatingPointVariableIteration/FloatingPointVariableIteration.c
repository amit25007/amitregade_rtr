#include <stdio.h>
int main(void)
{
	//variable declarations
	float floatNumber;
	float incrementFloatNumber = 1.7f; //simply change this value ONLY to get different outputs...

	//code
	printf("\n\n");

	printf("Printing Numbers %f to %f : \n\n", incrementFloatNumber, (incrementFloatNumber * 10.0f));

	floatNumber = incrementFloatNumber;
	do
	{
		printf("\t%f\n", floatNumber);
		floatNumber = floatNumber + incrementFloatNumber;
	}while (floatNumber <= (incrementFloatNumber * 10.0f));

	printf("\n\n");

	_getch();
	return(0);
}

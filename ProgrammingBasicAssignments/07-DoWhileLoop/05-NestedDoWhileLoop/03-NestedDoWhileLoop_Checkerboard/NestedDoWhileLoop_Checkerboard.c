#include <stdio.h>
int main(void)
{
	//variable declarations
	int i, j, check;

	//code
	printf("\n\n");

	i = 0;
	do
	{
		j = 0;
		do
		{
			check = ((i & 0x8) == 0) ^ ((j & 0x8) == 0);

			if (check == 0)
				printf("  ");

			if (check == 1)
				printf("* ");

			j++;

		}while (j < 64);
		printf("\n\n");
		i++;
	}while (i < 64);

	_getch();
	return(0);
}

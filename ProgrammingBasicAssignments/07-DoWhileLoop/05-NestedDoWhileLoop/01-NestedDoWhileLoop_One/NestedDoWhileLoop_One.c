#include <stdio.h>
int main(void)
{
	//variable declarations
	int num1, num2;

	//code
	printf("\n\n");

	num1 = 1;
	do
	{
		printf("Num1 = %d\n", num1);
		printf("--------\n\n");
		
		num2 = 1;
		do
		{
			printf("\tj = %d\n", num2);
			num2++;
		}while (num2 <= 5);
		num1++;
		printf("\n\n");
	}while (num1 <= 10);

	_getch();
	return(0);
}

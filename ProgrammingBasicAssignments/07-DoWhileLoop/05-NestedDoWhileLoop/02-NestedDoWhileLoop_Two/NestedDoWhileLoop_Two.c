#include <stdio.h>
int main(void)
{
	//variable declarations
	int number1, number2, number3;

	//code
	printf("\n\n");
	
	number1 = 1;
	do
	{
		printf("i = %d\n", number1);
		printf("--------\n\n");
		
		number2 = 1;
		do
		{
			printf("\tj = %d\n", number2);
			printf("\t--------\n\n");
			
			number3 = 1;
			do
			{
				printf("\t\tk = %d\n", number3);
				number3++;
			}while (number3 <= 3);
			printf("\n\n");
			number2++;
		}while (number2 <= 5);
		printf("\n\n");
		number1++;
	}while (number1 <= 10);

	_getch();
	return(0);
}

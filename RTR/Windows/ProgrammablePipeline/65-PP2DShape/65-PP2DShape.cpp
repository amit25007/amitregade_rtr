#include<windows.h>
#include<stdio.h>
#include<gl/glew.h>
#include<gl/GL.h>
#include<vmath.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glew32.lib")
using namespace vmath;

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

HDC ghdc = NULL;
HGLRC ghrc = NULL; //openGL rendoring context
DWORD dwstyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;
FILE *gpFile = NULL;
FILE *gpErrorLog = NULL;
bool bDone = false;
bool gbActiveWindow = false;
bool bIsFullScreen = false;

//Programmable pipeline objects
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void ToggleFullScreen(void);
void resize(int, int);
void uninitialize();

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

GLuint vao_triangle, vao_rectangle;
GLuint vbo_position_triangle, vbo_position_rectangle;
GLuint mvpUniform;
mat4 perspectiveProjectionMatrix;

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	int initialize();

	//fun declaration for double buffer window
	void display();

	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("64-PPPerspectiveTriangle");
	bool bDone = false;
	int iRet;

	//code 
	//Log file creation
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log file is created successfully\n");
	}

	//initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; //CS_OWNDC: To tell OS that this DC is specialized DC, hence it should not be discarded
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	//register wndclass
	RegisterClassEx(&wndclass);

	//create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("64-PPPerspectiveTriangle"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		75,
		75,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{

		fprintf(gpFile, "SetPixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "WglCreateContext() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "WglMakeCurrent() Failed \n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "initialize() function succeeded \n");
	}

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//game loop
	while (bDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				bDone = true;
			}
			else {
				//msg loop
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			//play game
			if (gbActiveWindow == true) {

			}
			display();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
	case WM_ERASEBKGND:
		return (0);
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
			ToggleFullScreen();
			break;
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


int initialize(void)
{
	void resize(int, int);

	//variables
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	GLenum result;

	//initialize 
	ZeroMemory((void *)&pfd, sizeof(PIXELFORMATDESCRIPTOR));		//initialize pfd structure and pfd members to 0
	//memset((void *)&pfd, NULL, size(PIXELFORMATDESCRIPTOR));		//another way

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;			//for 3d geometry, intialize depth buffer

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return (-1);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		return (-2);
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		return (-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		return (-4);
	}

	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf(gpFile, "glewInit failed\n");
		uninitialize();
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "glewInit succeeded\n");
	}

	//define vertex shader object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex shader code
	const GLchar *vertexShaderSourceCode = "#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix*vPosition;" \
		"}";

	//specify above source code object to the vertex shader object
	glShaderSource(gVertexShaderObject,
		1,			//no of strings
		(const GLchar **)&vertexShaderSourceCode,
		NULL);				//in case of multiple strings, provide array of lenght of each string

	//compile vertex shader
	glCompileShader(gVertexShaderObject);

	//Error checking
	if (fopen_s(&gpErrorLog, "ErrorLog.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("ErrorLog file cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLenght = 0;
	GLchar *szInfoLog = NULL;
	GLsizei written;

	//to get compile status of vertex shader
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLenght);

		if (iInfoLogLenght > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLenght);

			if (szInfoLog != NULL)
			{
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLenght, &written, szInfoLog);
				fprintf(gpErrorLog, "Vertex Shader compilation error\n");
				fprintf(gpErrorLog, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}

		}
	}

	//Fragment Shader code

	//define Fragment shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment shader code
	const GLchar *fragmentShaderSourceCode = "#version 450 core" \
		"\n" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
		"fragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
		"}";

	//specify above source code object to the Fragment shader object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	//compile Fragment shader
	glCompileShader(gFragmentShaderObject);

	//Error checking Fragment shader
	//to get compile status of Fragment shader
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		fprintf(gpErrorLog, "Fragment Shader compilation error\n");
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLenght);
		if (iInfoLogLenght > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLenght);

			if (szInfoLog != NULL)
			{
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLenght, &written, szInfoLog);
				fprintf(gpErrorLog, "Fragment Shader compilation error\n");
				fprintf(gpErrorLog, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//Create shader program object
	gShaderProgramObject = glCreateProgram();

	//Attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//Attach fragment shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//Prelinking binding to vertex attributes
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");


	//link the shader program
	glLinkProgram(gShaderProgramObject);

	//Error checking for linking
	GLint iProgramLinkStatus = 0;
	iInfoLogLenght = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLenght);
		if (iInfoLogLenght > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLenght);
			if (szInfoLog != NULL)
			{
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLenght, &written, szInfoLog);
				fprintf(gpErrorLog, "Program linking error\n");
				fprintf(gpErrorLog, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	const GLfloat triangleVertices[] = { 0.0f, 1.0f, 0.0f, -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f };
	const GLfloat rectangleVertices[] = { -1.0f, 1.0f, 0.0f, -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f, 1.0f, 1.0f, 0.0f };


	//create vao for triangle
	glGenVertexArrays(1, &vao_triangle);
	glBindVertexArray(vao_triangle);
	glGenBuffers(1, &vbo_position_triangle);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_triangle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//create vao for rectangle
	glGenVertexArrays(1, &vao_rectangle);
	glBindVertexArray(vao_rectangle);
	glGenBuffers(1, &vbo_position_rectangle);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_rectangle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleVertices), rectangleVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//give existance to depth buffer
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);		//adding depth test
	glDepthFunc(GL_LEQUAL);			//test

	glEnable(GL_TEXTURE_2D);

	perspectiveProjectionMatrix = mat4::identity();
	resize(WIN_WIDTH, WIN_HEIGHT);
	return (0);
}

void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);

}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	//Declaration of matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	//Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//Do necessary transformation
	modelViewMatrix = translate(-1.5f, 0.0f, -6.0f);

	//Do necessary matrix multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	//It was done by gluortho2d in fft

	//Send necessary matrices to shader in respective format
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,		//if we want to transpose the matrix
		modelViewProjectionMatrix);


	//Bind with vao
	glBindVertexArray(vao_triangle);

	//Similarly bind with textures if any

	//Draw necessary scene
	glDrawArrays(GL_TRIANGLES,
		0,		//from which position array will start
		3);		//

	//unbind vao
	glBindVertexArray(0);

	//rectangle
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//Do necessary transformation
	modelViewMatrix = translate(1.5f, 0.0f, -6.0f);

	//Do necessary matrix multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	//It was done by gluortho2d in fft

	//Send necessary matrices to shader in respective format
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,		//if we want to transpose the matrix
		modelViewProjectionMatrix);


	//Bind with vao
	glBindVertexArray(vao_rectangle);

	//Similarly bind with textures if any

	//Draw necessary scene
	glDrawArrays(GL_TRIANGLE_FAN,
		0,		//from which position array will start
		4);		//

	//unbind vao
	glBindVertexArray(0);

	//Unuse program
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void uninitialize()
{
	if (bIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwstyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	//safe uninitialize
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "fclose Successful");
		fclose(gpFile);
		gpFile = NULL;
	}
	if (gpErrorLog)
	{
		fprintf(gpErrorLog, "fclose Successful");
		fclose(gpErrorLog);
		gpErrorLog = NULL;
	}

	//release programmable pipleline objects
	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);

	if (vbo_position_triangle)
	{
		glDeleteBuffers(1, &vbo_position_triangle);
		vbo_position_triangle = 0;
	}

	if (vbo_position_rectangle)
	{
		glDeleteBuffers(1, &vbo_position_rectangle);
		vbo_position_rectangle = 0;
	}

	if (vao_triangle)
	{
		glDeleteVertexArrays(1, &vao_triangle);
	}
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen == false)
	{
		dwstyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwstyle & WS_OVERLAPPEDWINDOW) {
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, dwstyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		bIsFullScreen = true;
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwstyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bIsFullScreen = false;
	}
}


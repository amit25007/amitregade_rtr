#include<windows.h>
#include<stdio.h>
#include<gl/glew.h>
#include<gl/GL.h>
#include "vmath.h"
#include "Sphere.h"

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"Sphere.lib")

using namespace vmath;

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//sphere variables
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

unsigned short gNumElements, gNumVertices;

HDC ghdc = NULL;
HGLRC ghrc = NULL; //openGL rendering context
DWORD dwstyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;
FILE *gpFile = NULL;
FILE *gpErrorLog = NULL;
bool bDone = false;
bool gbActiveWindow = false;
bool bIsFullScreen = false;
bool gpLighting = false;
bool rotateCube = false;
static bool lightx = false, lighty = false, lightz = true;

//Programmable pipeline objects
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLfloat static rotateAngle = 0.0f;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void ToggleFullScreen(void);
void resize(int, int);
void uninitialize();

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

GLuint vao_sphere;
GLuint vbo_sphere_position;
GLuint vbo_sphere_normal, vbo_sphere_element;
GLuint modelUniform;
GLuint viewUniform;
GLuint projectionUniform;
GLuint u_laUniform, u_ldUniform, u_lsUniform;
GLuint u_kaUniform, u_kdUniform, u_ksUniform, u_lightPositionUniform; \
GLfloat u_shininessUniform;
GLuint lkeyIsPressed;
mat4 perspectiveProjectionMatrix;
mat4 modelMatrix, viewMatrix, projectionMatrix;

//values
GLfloat light_ambient[4] = { 0.0f, 0.0f , 0.0f, 0.0f };
GLfloat light_diffuse[4] = { 1.0f, 1.0f , 1.0f, 1.0f };
GLfloat light_specular[4] = { 1.0f, 1.0f , 1.0f, 1.0f }; 
GLfloat light_position[4] = { 100.0f, 100.0f, 100.0f, 1.0f };

GLfloat material_ambient[4] = { 0.0f, 0.0f , 0.0f, 0.0f };
GLfloat material_diffuse[4] = { 1.0f, 1.0f , 1.0f, 1.0f };
GLfloat material_specular[4] = { 1.0f, 1.0f , 1.0f, 1.0f };

GLfloat material_shineness[1] = { 50.0f };

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	int initialize();

	//fun declaration for double buffer window
	void display();
	void update();

	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("70_PPLightonSphere");
	bool bDone = false;
	int iRet;

	//code 
	//Log file creation
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log file is created successfully\n");
	}

	//initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; //CS_OWNDC: To tell OS that this DC is specialized DC, hence it should not be discarded
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	//register wndclass
	RegisterClassEx(&wndclass);

	//create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("70_PPLightonSphere"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		75,
		75,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{

		fprintf(gpFile, "SetPixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "WglCreateContext() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "WglMakeCurrent() Failed \n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "initialize() function succeeded \n");
	}

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//game loop
	while (bDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				bDone = true;
			}
			else {
				//msg loop
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			//play game
			if (gbActiveWindow == true) {

			}
			if (rotateCube == true) {
				update();
			}
			display();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
	case WM_ERASEBKGND:
		return (0);
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'a':
		case 'A':
			ToggleFullScreen();
			rotateCube = true;
			break;
		case 'x':
			if (lightx)
				lightx = false;
			else
				lightx = true;
			break;
		case 'y':
			if (lighty)
				lighty = false;
			else
				lighty = true;
			break;
		case 'z':
			if (lightz)
				lightz = false;
			else
				lightz = true;
			break;
		case 'l':
		case 'L':
			if (gpLighting == false)
			{
				gpLighting = true;
			}
			else {
				gpLighting = false;
			}
			break;
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int initialize(void)
{
	void resize(int, int);

	//variables
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	GLenum result;

	//initialize 
	ZeroMemory((void *)&pfd, sizeof(PIXELFORMATDESCRIPTOR));		//initialize pfd structure and pfd members to 0
	//memset((void *)&pfd, NULL, size(PIXELFORMATDESCRIPTOR));		//another way

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;			//for 3d geometry, intialize depth buffer

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return (-1);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		return (-2);
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		return (-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		return (-4);
	}

	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf(gpFile, "glewInit failed\n");
		uninitialize();
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "glewInit succeeded\n");
	}

	//define vertex shader object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex shader code
	const GLchar *vertexShaderSourceCode = "#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_modelMatrix;" \
		"uniform mat4 u_viewMatrix;" \
		"uniform mat4 u_projectionMatrix;" \
		"uniform int u_lKeyIsPressed;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_shininess;" \
		"uniform vec4 u_light_position;" \
		"out vec3 phong_ads_light;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyIsPressed==1)" \
		"	{" \
		/*step1: calculate eye co-ordinate, eye coordinate has no dependancy on projection matrix,
		projection coordinates we get from environment*/
		"		vec4 eye_coordinate = u_viewMatrix * u_modelMatrix * vPosition;" \

		/*Step2: calculate normal matrix
		normalMatrix = it is inverse transpose of the upper left 3*3 model view matrix*/
		"		mat3 normal_matrix = mat3(transpose(inverse(u_viewMatrix * u_modelMatrix)));" \

		/* step3: calculate transformed normal */
		"		vec3 T_Norm = normalize(normal_matrix * vNormal);" \

		/* Step4: calculate source vector
		s- source light direction(light direction)*/
		"		vec3 s = normalize(vec3(u_light_position - eye_coordinate));" \

		//calculate ads light
		"		vec3 tn_dot_ld = max(u_ls * u_kd * dot(s, T_Norm), 0);" \
		"		vec3 reflection_vector = reflect(-s, T_Norm);" \
		"		vec3 viewer_vector = normalize(vec3(-eye_coordinate.xyz));" \

		"		vec3 ambient = vec3(u_la * u_ka);" \
		"		vec3 diffuse = vec3(u_ld * u_kd * tn_dot_ld);" \
		"		vec3 specular = vec3(u_ls * u_ks * pow(max(dot(reflection_vector, viewer_vector), 0), u_shininess));" \

		/*step5: calculate amount of diffusion*/
		"		phong_ads_light = ambient + diffuse + specular;" \

		/* max : macro which returns maximum value from fun,
		e.g. if fun returns negative then max returns 0 instead*/

		"	}" \
		"	else {" \
		"		phong_ads_light = vec3(1.0f, 1.0f, 1.0f);" \
		"	}" \
		"	gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;" \
		"}";

	//specify above source code object to the vertex shader object
	glShaderSource(gVertexShaderObject,
		1,			//no of strings
		(const GLchar **)&vertexShaderSourceCode,
		NULL);				//in case of multiple strings, provide array of lenght of each string

	//compile vertex shader
	glCompileShader(gVertexShaderObject);

	//Error checking
	if (fopen_s(&gpErrorLog, "ErrorLog.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("ErrorLog file cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLenght = 0;
	GLchar *szInfoLog = NULL;
	GLsizei written;

	//to get compile status of vertex shader
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLenght);

		if (iInfoLogLenght > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLenght);

			if (szInfoLog != NULL)
			{
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLenght, &written, szInfoLog);
				fprintf(gpErrorLog, "Vertex Shader compilation error\n");
				fprintf(gpErrorLog, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}

		}
	}

	//Fragment Shader code

	//define Fragment shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment shader code
	const GLchar *fragmentShaderSourceCode = "#version 450 core" \
		"\n" \
		"in vec3 phong_ads_light;" \
		"uniform int u_lKeyIsPressed;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
	
		"		fragColor = vec4(phong_ads_light, 1.0);" \
	
		"}";

	//specify above source code object to the Fragment shader object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	//compile Fragment shader
	glCompileShader(gFragmentShaderObject);

	//Error checking Fragment shader
	//to get compile status of Fragment shader
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLenght);
		if (iInfoLogLenght > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLenght);

			if (szInfoLog != NULL)
			{
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLenght, &written, szInfoLog);
				fprintf(gpErrorLog, "Fragment Shader compilation error\n");
				fprintf(gpErrorLog, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//Create shader program object
	gShaderProgramObject = glCreateProgram();

	//Attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//Attach fragment shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//Prelinking binding to vertex attributes
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//link the shader program
	glLinkProgram(gShaderProgramObject);

	//Error checking for linking
	GLint iProgramLinkStatus = 0;
	iInfoLogLenght = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLenght);
		if (iInfoLogLenght > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLenght);
			if (szInfoLog != NULL)
			{
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLenght, &written, szInfoLog);
				fprintf(gpErrorLog, "Program linking error\n");
				fprintf(gpErrorLog, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	modelUniform = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
	viewUniform = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
	projectionUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");
	lkeyIsPressed = glGetUniformLocation(gShaderProgramObject, "u_lKeyIsPressed");
	u_laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	u_ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	u_lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	u_kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	u_kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	u_ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	u_lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	u_shininessUniform = glGetUniformLocation(gShaderProgramObject, "u_shininess");
	

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();



	//create vao for rectangle
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	//bind cube position
	glGenBuffers(1, &vbo_sphere_position);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//bind sphere normal
	glGenBuffers(1, &vbo_sphere_normal);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//bind sphere element
	glGenBuffers(1, &vbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//unbind vao
	glBindVertexArray(0);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//give existance to depth buffer
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);		//adding depth test
	glDepthFunc(GL_LEQUAL);			//test
	glEnable(GL_TEXTURE_2D);
	perspectiveProjectionMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
	return (0);
}

void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	//Declaration of matrices

	mat4 translateMatrix;
	mat4 scaleMatrix;
	mat4 rotationMatrix;

	//Initialize above matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	translateMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	//Do necessary transformation
	translateMatrix = translate(0.0f, 0.0f, -4.0f);
	rotationMatrix = rotate(rotateAngle, rotateAngle, rotateAngle);	//for arbitory axis use this fun
	//scaleMatrix = scale(0.75f, 0.75f, 0.75f);
	modelMatrix = modelMatrix * translateMatrix * rotationMatrix * scaleMatrix;

	//Do necessary matrix multiplication
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;
	//It was done by gluortho2d in fft

	//Send necessary matrices to shader in respective format
	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, projectionMatrix);

	//send position matrix
	if (gpLighting == true)
	{
		//send all lighting uniforms
		glUniform1i(lkeyIsPressed, 1);

		//send vec3 uniforms
		glUniform3f(u_laUniform, light_ambient[0], light_ambient[1], light_ambient[2]);
		glUniform3f(u_ldUniform, light_diffuse[0], light_diffuse[1], light_diffuse[2]);
		glUniform3f(u_lsUniform, light_specular[0], light_specular[1], light_specular[2] );
		glUniform3f(u_kaUniform, material_ambient[0], material_ambient[1], material_ambient[2]);
		glUniform3f(u_kdUniform, material_diffuse[0], material_diffuse[1], material_diffuse[2]);
		glUniform3f(u_ksUniform, material_specular[0], material_specular[1], material_specular[2]);

		//send vec4 uniforms
		glUniform4fv(u_lightPositionUniform, 1, light_position);

		glUniform1f(u_shininessUniform, material_shineness[0]);
		
	}
	else
	{
		glUniform1i(lkeyIsPressed, 0);
	}

	//Bind with vao
	glBindVertexArray(vao_sphere);

	//Similarly bind with textures if any
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);


	//Draw necessary scene
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//unbind vao
	glBindVertexArray(0);

	//Unuse program
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void update()
{
	rotateAngle = rotateAngle + 0.2f;
	if (rotateAngle >= 360.0f)
	{
		rotateAngle = 0.0f;
	}
}

void uninitialize()
{
	if (bIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwstyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	//safe uninitialize
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "fclose Successful");
		fclose(gpFile);
		gpFile = NULL;
	}
	if (gpErrorLog)
	{
		fprintf(gpErrorLog, "fclose Successful");
		fclose(gpErrorLog);
		gpErrorLog = NULL;
	}

	//release programmable pipleline objects
	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);

	if (vbo_sphere_position)
	{
		glDeleteBuffers(1, &vbo_sphere_position);
		vbo_sphere_position = 0;
	}

	if (vbo_sphere_normal)
	{
		glDeleteBuffers(1, &vbo_sphere_normal);
		vbo_sphere_normal = 0;
	}

	if (vbo_sphere_element)
	{
		glDeleteBuffers(1, &vbo_sphere_element);
		vbo_sphere_element = 0;
	}

	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
	}
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen == false)
	{
		dwstyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwstyle & WS_OVERLAPPEDWINDOW) {
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, dwstyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		bIsFullScreen = true;
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwstyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bIsFullScreen = false;
	}
}


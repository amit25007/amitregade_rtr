#include<windows.h>
#include<stdio.h>
#include<gl/glew.h>
#include<gl/GL.h>
#include<vmath.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glew32.lib")
using namespace vmath;

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define PI 3.141592653589793

HDC ghdc = NULL;
HGLRC ghrc = NULL; //openGL rendoring context
DWORD dwstyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;
FILE *gpFile = NULL;
FILE *gpErrorLog = NULL;
bool bDone = false;
bool gbActiveWindow = false;
bool bIsFullScreen = false;

//Programmable pipeline objects
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

//circle variables
const int numpoints = 1000;

//co-ordinate variables
GLfloat vx1 = -0.785f, vy1 = 0.25f;
GLfloat width = 0.06f;
GLfloat gap = 0.08f;
GLfloat letterStretch = 0.3f;

static GLfloat rotateAngle = 0.0f;
static GLfloat speed = 0.02f;
static bool rotateGeometry = false;



LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void ToggleFullScreen(void);
void resize(int, int);
void uninitialize();

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

GLuint vao_i, vao_n, vao_d, vao_dc, vao_i2, vao_a;
GLuint vbo_position_i, vbo_position_n, vbo_position_d, vbo_position_dc, vbo_position_i2, vbo_position_a;
GLuint vbo_color_i, vbo_color_n, vbo_color_d, vbo_color_dc, vbo_color_i2, vbo_color_a;
GLuint mvpUniform;
mat4 perspectiveProjectionMatrix;

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	int initialize();

	//fun declaration for double buffer window
	void display();

	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("78_StaticIndia");
	bool bDone = false;
	int iRet;

	//code 
	//Log file creation
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log file is created successfully\n");
	}

	//initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; //CS_OWNDC: To tell OS that this DC is specialized DC, hence it should not be discarded
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	//register wndclass
	RegisterClassEx(&wndclass);

	//create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("78_StaticIndia"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		75,
		75,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{

		fprintf(gpFile, "SetPixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "WglCreateContext() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "WglMakeCurrent() Failed \n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "initialize() function succeeded \n");
	}

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//game loop
	while (bDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				bDone = true;
			}
			else {
				//msg loop
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			//play game
			if (gbActiveWindow == true) {

			}
			display();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
	case WM_ERASEBKGND:
		return (0);
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
			ToggleFullScreen();
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'r':
		case 'R':
			rotateGeometry = true;
			break;
		case 'd':
		case 'D':
			speed = speed + 0.03f;
			break;
		case 's':
		case 'S':
			speed = speed - 0.03f;
			break;
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int initialize(void) {
	void resize(int, int);

	//variables
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	GLenum result;

	//initialize 
	ZeroMemory((void *)&pfd, sizeof(PIXELFORMATDESCRIPTOR));		//initialize pfd structure and pfd members to 0
	//memset((void *)&pfd, NULL, size(PIXELFORMATDESCRIPTOR));		//another way

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;			//for 3d geometry, intialize depth buffer

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return (-1);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		return (-2);
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		return (-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		return (-4);
	}

	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf(gpFile, "glewInit failed\n");
		uninitialize();
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "glewInit succeeded\n");
	}

	//define vertex shader object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex shader code
	const GLchar *vertexShaderSourceCode = "#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_color;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix*vPosition;" \
		"out_color = vColor;" \
		"}";

	//specify above source code object to the vertex shader object
	glShaderSource(gVertexShaderObject,
		1,			//no of strings
		(const GLchar **)&vertexShaderSourceCode,
		NULL);				//in case of multiple strings, provide array of lenght of each string

	//compile vertex shader
	glCompileShader(gVertexShaderObject);

	//Error checking
	if (fopen_s(&gpErrorLog, "ErrorLog.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("ErrorLog file cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLenght = 0;
	GLchar *szInfoLog = NULL;
	GLsizei written;

	//to get compile status of vertex shader
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLenght);

		if (iInfoLogLenght > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLenght);

			if (szInfoLog != NULL)
			{
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLenght, &written, szInfoLog);
				fprintf(gpErrorLog, "Vertex Shader compilation error\n");
				fprintf(gpErrorLog, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}

		}
	}

	//Fragment Shader code

	//define Fragment shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment shader code
	const GLchar *fragmentShaderSourceCode = "#version 450 core" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
		"fragColor = out_color;" \
		"}";

	//specify above source code object to the Fragment shader object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	//compile Fragment shader
	glCompileShader(gFragmentShaderObject);

	//Error checking Fragment shader
	//to get compile status of Fragment shader
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		fprintf(gpErrorLog, "Fragment Shader compilation error\n");
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLenght);
		if (iInfoLogLenght > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLenght);

			if (szInfoLog != NULL)
			{
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLenght, &written, szInfoLog);
				fprintf(gpErrorLog, "Fragment Shader compilation error\n");
				fprintf(gpErrorLog, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//Create shader program object
	gShaderProgramObject = glCreateProgram();

	//Attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//Attach fragment shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//Prelinking binding to vertex attributes
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");


	//link the shader program
	glLinkProgram(gShaderProgramObject);

	//Error checking for linking
	GLint iProgramLinkStatus = 0;
	iInfoLogLenght = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLenght);
		if (iInfoLogLenght > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLenght);
			if (szInfoLog != NULL)
			{
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLenght, &written, szInfoLog);
				fprintf(gpErrorLog, "Program linking error\n");
				fprintf(gpErrorLog, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	const GLfloat IPosition[] = {
		vx1, vy1, vx1, -vy1, vx1 + width, -vy1, vx1 + width, vy1
	};

	GLfloat vxN = vx1 + width + gap;
	const GLfloat NPosition[] = {
		vxN, vy1, vxN, -vy1, vxN + width, -vy1,	vxN + width, vy1,
		vxN, vy1, vxN + letterStretch, -vy1, vxN + width + letterStretch, -vy1,	vxN + width, vy1,
		vxN + letterStretch, vy1, vxN + letterStretch, -vy1, vxN + letterStretch + width, -vy1, vxN + letterStretch + width, vy1
	};

	GLfloat vxD = vxN + width + gap + letterStretch;
	const GLfloat DPosition[] = {
		vxD, vy1,  vxD, -vy1, vxD + width, -vy1, vxD + width, vy1
	};

	const size_t nVertices = 126;
	GLfloat DCurvePosition[2 * nVertices];
	GLfloat DCurveColor[3 * nVertices];

	GLfloat angle = (GLfloat)PI / 2.0f;
	GLint counter = 0;
	for (GLfloat i = 0.0f; i < 2.0*PI; i = i + 0.05f)
	{
		if (i <= PI)
		{
			for (int j = 0; j < 10; j++);
			{
				GLfloat move = 0.0f;
				DCurveColor[3 * counter]= 1.0f;
				DCurveColor[3 * counter + 1] = 0.6f;
				DCurveColor[3 * counter + 2] = 0.2f;
				DCurvePosition[2 * counter] = vxD + width + cos(angle)*vy1 - move;
				DCurvePosition[2 * counter+1] = sin(angle)*vy1 - move;
				move = move + 0.01f;
			}
			angle = angle - 0.001f;
		}
		else
		{
			if (i <= 2.0f*PI)
			{
				for (int k = 0; k < 10; k++)
				{
					GLfloat move = 0.0f;
					DCurveColor[3 * counter] = 0.0742f;
					DCurveColor[3 * counter + 1] = 0.5333f;
					DCurveColor[3 * counter + 2] = 0.3137f;
					DCurvePosition[2 * counter] = vxD + width + cos(angle)*(vy1)+move;
					DCurvePosition[2 * counter + 1] = sin(angle)*(vy1)+move;
					move = move + 0.01f;
				}
			}
			//glColor3f(1.0f, 0.6f, 0.2f);
			//glVertex3f(vxD + width + cos(angle)*(vy1 - width), sin(angle)*(vy1 - width), 0.0f);
			angle = angle + 0.001f;
		}
		counter = counter + 1;
	}



	GLfloat vxI = vxD + width + gap + vy1;
	const GLfloat I2Position[] = {
		vxI, vy1, vxI, -vy1, vxI + width, -vy1, vxI + width, vy1
	};

	GLfloat vxA = vxI + width + gap;
	letterStretch = 0.2f;
	const GLfloat APosition[] = {
		vxA + letterStretch, vy1, vxA, -vy1, vxA + width, -vy1, vxA + width + letterStretch, vy1,
		vxA + letterStretch, vy1, vxA + letterStretch * 2.0f, -vy1, vxA + letterStretch * 2.0f + width, -vy1, vxA + width + letterStretch, vy1
	};


	const GLfloat iColor[] = {
		1.0f, 0.6f, 0.2f, 0.0742f, 0.5333f, 0.3137f, 0.0742f, 0.5333f, 0.3137f, 1.0f, 0.6f, 0.2f
	};

	const GLfloat nColor[] = {
		1.0f, 0.6f, 0.2f, 0.0742f, 0.5333f, 0.3137f, 0.0742f, 0.5333f, 0.3137f, 1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f, 0.0742f, 0.5333f, 0.3137f, 0.0742f, 0.5333f, 0.3137f, 1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f, 0.0742f, 0.5333f, 0.3137f, 0.0742f, 0.5333f, 0.3137f, 1.0f, 0.6f, 0.2f
	};

	const GLfloat dColor[] = {
		1.0f, 0.6f, 0.2f, 0.0742f, 0.5333f, 0.3137f, 0.0742f, 0.5333f, 0.3137f, 1.0f, 0.6f, 0.2f
	};

	const GLfloat i2Color[] = {
		1.0f, 0.6f, 0.2f, 0.0742f, 0.5333f, 0.3137f, 0.0742f, 0.5333f, 0.3137f, 1.0f, 0.6f, 0.2f
	};

	const GLfloat aColor[] = {
		1.0f, 0.6f, 0.2f, 0.0742f, 0.5333f, 0.3137f, 0.0742f, 0.5333f, 0.3137f, 1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f, 0.0742f, 0.5333f, 0.3137f, 0.0742f, 0.5333f, 0.3137f, 1.0f, 0.6f, 0.2f
	};

	//create vao for I
	glGenVertexArrays(1, &vao_i);
	glBindVertexArray(vao_i);

	//I position bind
	glGenBuffers(1, &vbo_position_i);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_i);
	glBufferData(GL_ARRAY_BUFFER, sizeof(IPosition), IPosition, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//I color bind
	glGenBuffers(1, &vbo_color_i);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_i);
	glBufferData(GL_ARRAY_BUFFER, sizeof(iColor), iColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//create vao for N
	glGenVertexArrays(1, &vao_n);
	glBindVertexArray(vao_n);

	//bind rectangle position
	glGenBuffers(1, &vbo_position_n);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_n);
	glBufferData(GL_ARRAY_BUFFER, sizeof(NPosition), NPosition, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//bind n color
	glGenBuffers(1, &vbo_position_n);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_n);
	glBufferData(GL_ARRAY_BUFFER, sizeof(nColor), nColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//create vao for D
	glGenVertexArrays(1, &vao_d);
	glBindVertexArray(vao_d);

	//D position bind
	glGenBuffers(1, &vbo_position_d);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_d);
	glBufferData(GL_ARRAY_BUFFER, sizeof(DPosition), DPosition, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//D color bind
	glGenBuffers(1, &vbo_color_d);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_d);
	glBufferData(GL_ARRAY_BUFFER, sizeof(dColor), dColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//create vao for Dc
	glGenVertexArrays(1, &vao_dc);
	glBindVertexArray(vao_dc);

	//D position bind
	glGenBuffers(1, &vbo_position_dc);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_dc);
	glBufferData(GL_ARRAY_BUFFER, sizeof(DCurvePosition), DCurvePosition, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//D color bind
	glGenBuffers(1, &vbo_color_dc);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_dc);
	glBufferData(GL_ARRAY_BUFFER, sizeof(DCurveColor), DCurveColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//create vao for I2
	glGenVertexArrays(1, &vao_i2);
	glBindVertexArray(vao_i2);

	//I2 position bind
	glGenBuffers(1, &vbo_position_i2);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_i2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(I2Position), I2Position, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//I2 color bind
	glGenBuffers(1, &vbo_color_i2);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_i2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(i2Color), i2Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//create vao for A
	glGenVertexArrays(1, &vao_a);
	glBindVertexArray(vao_a);

	//A position bind
	glGenBuffers(1, &vbo_position_a);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_a);
	glBufferData(GL_ARRAY_BUFFER, sizeof(APosition), APosition, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//A color bind
	glGenBuffers(1, &vbo_color_a);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_a);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aColor), aColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//comment


	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_CULL_FACE_MODE);

	//give existance to depth buffer
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);		//adding depth test
	glDepthFunc(GL_LEQUAL);			//test

	glEnable(GL_TEXTURE_2D);

	perspectiveProjectionMatrix = mat4::identity();
	resize(WIN_WIDTH, WIN_HEIGHT);
	return (0);
}

void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);

}

void display()
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	//Declaration of matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	mat4 translateMatrix;
	mat4 scaleMatrix;
	mat4 rotationMatrix;

	//Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	translateMatrix = mat4::identity();
	rotationMatrix = mat4::identity();

	//Do necessary transformation
	translateMatrix = translate(0.0f, 0.0f, -3.0f);
	rotationMatrix = rotate(rotateAngle, 0.0f, 1.0f, 0.0f);
	modelViewMatrix = modelViewMatrix * translateMatrix * rotationMatrix;

	//Do necessary matrix multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	//It was done by gluortho2d in fft

	//Send necessary matrices to shader in respective format
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,		//if we want to transpose the matrix
		modelViewProjectionMatrix);


	//Bind with i
	glBindVertexArray(vao_i);

	//Similarly bind with textures if any

	//Draw necessary scene
	glDrawArrays(GL_QUADS,
		0,		//from which position array will start
		8);

	//unbind vao
	glBindVertexArray(0);

	//Bind with n
	glBindVertexArray(vao_n);

	//Similarly bind with textures if any

	//Draw necessary scene
	glDrawArrays(GL_QUADS,
		0,		//from which position array will start
		8);		//
	
	glDrawArrays(GL_QUADS,
		8,
		16);
	glDrawArrays(GL_QUADS,
		16,
		24);

	//unbind vao
	glBindVertexArray(0);
	
	//Bind with d
	glBindVertexArray(vao_d);

	//Similarly bind with textures if any

	//Draw necessary scene
	glDrawArrays(GL_QUADS,
		0,		//from which position array will start
		8);

	//unbind vao
	glBindVertexArray(0);

	//Bind with dcurve
	glBindVertexArray(vao_dc);

	//Similarly bind with textures if any

	//Draw necessary scene
	glDrawArrays(GL_POINTS,
		0,		//from which position array will start
		126);

	//unbind vao
	glBindVertexArray(0);

	//Bind with i2
	glBindVertexArray(vao_i2);

	//Similarly bind with textures if any

	//Draw necessary scene
	glDrawArrays(GL_QUADS,
		0,		//from which position array will start
		8);

	//unbind vao
	glBindVertexArray(0);

	//Bind with A
	glBindVertexArray(vao_a);

	//Similarly bind with textures if any

	//Draw necessary scene
	glDrawArrays(GL_QUADS,
		0,		//from which position array will start
		8);
	glDrawArrays(GL_QUADS,
		8,		//from which position array will start
		16);

	//unbind vao
	glBindVertexArray(0);

	//Unuse program
	glUseProgram(0);

	SwapBuffers(ghdc);
	/*
	rotateAngle = rotateAngle + 0.05f;
	if (rotateAngle >= 360.0f)
	{
		rotateAngle = 0.0f;
	}
	*/
}



void uninitialize()
{
	if (bIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwstyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	//safe uninitialize
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "fclose Successful");
		fclose(gpFile);
		gpFile = NULL;
	}
	if (gpErrorLog)
	{
		fprintf(gpErrorLog, "fclose Successful");
		fclose(gpErrorLog);
		gpErrorLog = NULL;
	}

	//release programmable pipleline objects
	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);

	if (vbo_position_i)
	{
		glDeleteBuffers(1, &vbo_position_i);
		vbo_position_i = 0;
	}
	if (vbo_color_i)
	{
		glDeleteBuffers(1, &vbo_color_i);
		vbo_color_i = 0;
	}
	if (vao_i)
	{
		glDeleteVertexArrays(1, &vao_i);
	}

	if (vbo_position_n)
	{
		glDeleteBuffers(1, &vbo_position_n);
		vbo_position_n = 0;
	}
	if (vbo_color_n)
	{
		glDeleteBuffers(1, &vbo_color_n);
		vbo_color_n = 0;
	}
	if (vao_n)
	{
		glDeleteVertexArrays(1, &vao_n);
	}

	if (vbo_position_d)
	{
		glDeleteBuffers(1, &vbo_position_d);
		vbo_position_d = 0;
	}
	if (vbo_color_d)
	{
		glDeleteBuffers(1, &vbo_color_d);
		vbo_color_d = 0;
	}
	if (vao_d)
	{
		glDeleteVertexArrays(1, &vao_d);
	}

	if (vbo_position_dc)
	{
		glDeleteBuffers(1, &vbo_position_dc);
		vbo_position_dc = 0;
	}
	if (vbo_color_dc)
	{
		glDeleteBuffers(1, &vbo_color_dc);
		vbo_color_dc = 0;
	}
	if (vao_dc)
	{
		glDeleteVertexArrays(1, &vao_dc);
	}

	if (vbo_position_i2)
	{
		glDeleteBuffers(1, &vbo_position_i2);
		vbo_position_i2 = 0;
	}
	if (vbo_color_i2)
	{
		glDeleteBuffers(1, &vbo_color_i2);
		vbo_color_i2 = 0;
	}
	if (vao_i2)
	{
		glDeleteVertexArrays(1, &vao_i2);
	}

	if (vbo_position_a)
	{
		glDeleteBuffers(1, &vbo_position_a);
		vbo_position_a = 0;
	}
	if (vbo_color_a)
	{
		glDeleteBuffers(1, &vbo_color_a);
		vbo_color_a = 0;
	}
	if (vao_a)
	{
		glDeleteVertexArrays(1, &vao_a);
	}
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen == false)
	{
		dwstyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwstyle & WS_OVERLAPPEDWINDOW) {
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, dwstyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		bIsFullScreen = true;
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwstyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bIsFullScreen = false;
	}
}


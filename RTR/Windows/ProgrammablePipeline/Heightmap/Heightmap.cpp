#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <stddef.h>
#include <gl/glew.h>
#include <gl/GL.h>
#include <vmath.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glew32.lib")
using namespace vmath;
#define DEBUG_ENABLED 1

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

/* Map height updates */
#define MAX_CIRCLE_SIZE (5.0f)
#define MAX_DISPLACEMENT (1.0f)
#define DISPLACEMENT_SIGN_LIMIT (0.3f)
#define MAX_ITER (200)
#define NUM_ITER_AT_A_TIME (1)

/* Map general information */
#define MAP_SIZE (100.0f)
#define MAP_NUM_VERTICES (300)
#define MAP_NUM_TOTAL_VERTICES (MAP_NUM_VERTICES*MAP_NUM_VERTICES)
#define MAP_NUM_LINES (3* (MAP_NUM_VERTICES - 1) * (MAP_NUM_VERTICES - 1) + \
               2 * (MAP_NUM_VERTICES - 1))

/**********************************************************************
 * Values for shader uniforms
 *********************************************************************/

 /* Frustum configuration */
static GLfloat view_angle = 45.0f;
static GLfloat aspect_ratio = 4.0f / 3.0f;
static GLfloat z_near = 1.0f;
static GLfloat z_far = 100.f;

static GLfloat tx = 0.0f, ty = 0.0f, tz = -3.5f;
static GLfloat rx = 0.0f, ry = 0.0f, rz = 0.0f;
GLfloat angleIncreament = 1.0f, moveIncreament = 0.25f;
static bool bPerspective = true;

mat4 projection_matrix;
mat4 modelview_matrix;
mat4 mvp_matrix;
mat4 translateMatrix;
mat4 rotateMatrix;
mat4 scaleMatrix;

mat4 perspectiveProjectionMatrix;

/**********************************************************************
 * Heightmap vertex and index data
 *********************************************************************/

static GLfloat map_vertices[3][MAP_NUM_TOTAL_VERTICES];
static GLuint map_line_indices[2 * MAP_NUM_LINES];
static GLuint map_textures[2][MAP_NUM_TOTAL_VERTICES];

/* Store uniform location for the shaders
 * Those values are setup as part of the process of creating
 * the shader program. They should not be used before creating
 * the program.
 */
static GLuint mesh_vao;
static GLuint mesh_vbo[4];


GLint uloc_modelview;
GLint uloc_project;

HDC ghdc = NULL;
HGLRC ghrc = NULL; //openGL rendoring context
DWORD dwstyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;
FILE *gpFile = NULL;
FILE *gpErrorLog = NULL;
bool bDone = false;
bool gbActiveWindow = false;
bool bIsFullScreen = false;

//Programmable pipeline objects
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void ToggleFullScreen(void);
void resize(int, int);
void uninitialize();

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	int initialize();

	//fun declaration for double buffer window
	void display();

	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Heightmap");
	bool bDone = false;
	int iRet;

	//code 
	//Log file creation
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log file is created successfully\n");
	}

	//initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; //CS_OWNDC: To tell OS that this DC is specialized DC, hence it should not be discarded
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	//register wndclass
	RegisterClassEx(&wndclass);

	//create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Heightmap"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		75,
		75,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{

		fprintf(gpFile, "SetPixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "WglCreateContext() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "WglMakeCurrent() Failed \n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "initialize() function succeeded \n");
	}

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//game loop
	while (bDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				bDone = true;
			}
			else {
				//msg loop
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			//play game
			if (gbActiveWindow == true) {

			}
			display();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
	case WM_ERASEBKGND:
		return (0);
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
			ToggleFullScreen();
			break;
		case VK_UP:
			ty = ty + moveIncreament;
			break;
		case VK_DOWN:
			ty = ty - moveIncreament;
			break;
		case VK_LEFT:
			tx = tx - moveIncreament;
			break;
		case VK_RIGHT:
			tx = tx + moveIncreament;
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case '4':
			ry = ry + angleIncreament;
			break;
		case '6':
			ry = ry - angleIncreament;
			break;
		case '8':
			rx = rx + angleIncreament;
			break;
		case '2':
			rx = rx - angleIncreament;
			break;
		case '1':
			tz = tz - moveIncreament;
			break;
		case '3':
			tz = tz + moveIncreament;
			break;
		case 'p':
			if (bPerspective)
			{
				bPerspective = false;
			}
			else {
				bPerspective = true;
			}
			break;
		default:
			break;
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int initialize(void)
{
	void resize(int, int);
	void init_map();

	//variables
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	GLenum result;

	//heightmap vars
	GLuint attrloc;
	
	//initialize 
	ZeroMemory((void *)&pfd, sizeof(PIXELFORMATDESCRIPTOR));		//initialize pfd structure and pfd members to 0
	//memset((void *)&pfd, NULL, size(PIXELFORMATDESCRIPTOR));		//another way

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;			//for 3d geometry, intialize depth buffer

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return (-1);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		return (-2);
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		return (-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		return (-4);
	}

	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf(gpFile, "glewInit failed\n");
		uninitialize();
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "glewInit succeeded\n");
	}

	//define vertex shader object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex shader code
	const GLchar *vertexShaderSourceCode = "#version 450 core" \
		"\n" \
		"in float x;" \
		"in float y;" \
		"in float z;" \
		"uniform mat4 projection;" \
		"uniform mat4 modelview;" \
		"void main(void)" \
		"{" \
		"	gl_Position = projection * modelview * vec4(x, y, z, 1.0);" \
		"}";

	//specify above source code object to the vertex shader object
	glShaderSource(gVertexShaderObject,
		1,			//no of strings
		(const GLchar **)&vertexShaderSourceCode,
		NULL);				//in case of multiple strings, provide array of lenght of each string

	//compile vertex shader
	glCompileShader(gVertexShaderObject);

	//Error checking
	if (fopen_s(&gpErrorLog, "ErrorLog.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("ErrorLog file cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLenght = 0;
	GLchar *szInfoLog = NULL;
	GLsizei written;

	//to get compile status of vertex shader
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLenght);

		if (iInfoLogLenght > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLenght);

			if (szInfoLog != NULL)
			{
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLenght, &written, szInfoLog);
				fprintf(gpErrorLog, "Vertex Shader compilation error\n");
				fprintf(gpErrorLog, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}

		}
	}

	//Fragment Shader code

	//define Fragment shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment shader code
	const GLchar *fragmentShaderSourceCode = "#version 450 core" \
		"\n" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
		"	fragColor = vec4(0.2, 1.0, 0.2, 1.0);" \
		"}";

	//specify above source code object to the Fragment shader object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	//compile Fragment shader
	glCompileShader(gFragmentShaderObject);

	//Error checking Fragment shader
	//to get compile status of Fragment shader
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLenght);
		if (iInfoLogLenght > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLenght);

			if (szInfoLog != NULL)
			{
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLenght, &written, szInfoLog);
				fprintf(gpErrorLog, "Fragment Shader compilation error\n");
				fprintf(gpErrorLog, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//Create shader program object
	gShaderProgramObject = glCreateProgram();

	//Attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//Attach fragment shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//link the shader program
	glLinkProgram(gShaderProgramObject);

	//Error checking for linking
	GLint iProgramLinkStatus = 0;
	iInfoLogLenght = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLenght);
		if (iInfoLogLenght > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLenght);
			if (szInfoLog != NULL)
			{
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLenght, &written, szInfoLog);
				fprintf(gpErrorLog, "Program linking error\n");
				fprintf(gpErrorLog, szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	uloc_project = glGetUniformLocation(gShaderProgramObject, "projection");
	uloc_modelview = glGetUniformLocation(gShaderProgramObject, "modelview");

	/* Create mesh data */
	init_map();

	//
	glGenVertexArrays(1, &mesh_vao);
	glBindVertexArray(mesh_vao);

	glGenBuffers(4, mesh_vbo);

	/* Prepare the data for drawing through a buffer inidices */
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh_vbo[3]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * MAP_NUM_LINES * 2, map_line_indices, GL_STATIC_DRAW);


	/* Prepare the attributes for rendering */
	attrloc = glGetAttribLocation(gShaderProgramObject, "x");
	glBindBuffer(GL_ARRAY_BUFFER, mesh_vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*MAP_NUM_TOTAL_VERTICES, &map_vertices[0][0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(attrloc);
	glVertexAttribPointer(attrloc, 1, GL_FLOAT, GL_FALSE, 0, 0);

	attrloc = glGetAttribLocation(gShaderProgramObject, "z");
	glBindBuffer(GL_ARRAY_BUFFER, mesh_vbo[2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * MAP_NUM_TOTAL_VERTICES, &map_vertices[2][0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(attrloc);
	glVertexAttribPointer(attrloc, 1, GL_FLOAT, GL_FALSE, 0, 0);

	attrloc = glGetAttribLocation(gShaderProgramObject, "y");
	glBindBuffer(GL_ARRAY_BUFFER, mesh_vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * MAP_NUM_TOTAL_VERTICES, &map_vertices[1][0], GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(attrloc);
	glVertexAttribPointer(attrloc, 1, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//give existance to depth buffer
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);		//adding depth test
	glDepthFunc(GL_LEQUAL);			//test

	glEnable(GL_TEXTURE_2D);

	perspectiveProjectionMatrix = mat4::identity();
	resize(WIN_WIDTH, WIN_HEIGHT);
	return (0);
}

void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}

void display()
{
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	/* Compute the projection matrix */
	/*
	float f;
	f = 1.0f / tanf(view_angle / 2.0f);
	projection_matrix[0] = f / aspect_ratio;
	projection_matrix[5] = f;
	projection_matrix[10] = (z_far + z_near) / (z_near - z_far);
	projection_matrix[11] = -1.0f;
	projection_matrix[14] = 2.0f * (z_far * z_near) / (z_near - z_far);
	*/

	projection_matrix = mat4::identity();
	modelview_matrix = mat4::identity();

	translateMatrix = mat4::identity();
	rotateMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	translateMatrix = translate(tx, ty, tz);
	rotateMatrix = rotate(rx, ry, rz);
	scaleMatrix = scale(0.2f);
	
	modelview_matrix = modelview_matrix * translateMatrix * rotateMatrix * scaleMatrix;
	if (bPerspective)
	{
		projection_matrix = perspectiveProjectionMatrix * projection_matrix;
	}
	else {
		projection_matrix = mat4::identity();
	}

	
	
	/* Set the camera position */
	/*
	modelview_matrix[12] = -5.0f;
	modelview_matrix[13] = -5.0f;
	modelview_matrix[14] = -20.0f;
	*/
	glUniformMatrix4fv(uloc_modelview, 1, GL_FALSE, modelview_matrix);
	glUniformMatrix4fv(uloc_project, 1, GL_FALSE, projection_matrix);

	//Bind with vao
	glBindVertexArray(mesh_vao);

	//Similarly bind with textures if any

	//Draw necessary scene
	glDrawElements(GL_LINES, 2 * MAP_NUM_LINES, GL_UNSIGNED_INT, 0);

	//unbind vao
	glBindVertexArray(0);

	//Unuse program
	glUseProgram(0);

	SwapBuffers(ghdc);
}

/**********************************************************************
 * Geometry creation functions
 *********************************************************************/

 /* Generate vertices and indices for the heightmap
  */
void init_map(void)
{
	int i;
	int j;
	int k;
	GLfloat step = (GLfloat) MAP_SIZE / ((GLfloat)MAP_NUM_VERTICES - 1);
	GLfloat dia = (GLfloat) MAP_SIZE / 3.0f;
	GLfloat elevationFactor = (GLfloat)MAP_SIZE * 0.05f;
	GLfloat angleStep = 180.0f * MAP_SIZE / (((GLfloat)MAP_NUM_VERTICES - 1)*dia);
	GLfloat yAngle = 0.0f, xTempAngle = 0.0f;
	GLfloat x = -(GLfloat)MAP_SIZE/2.0f;
	GLfloat y = 0.0f;
	GLfloat z = -(GLfloat)MAP_SIZE/2.0f;
	GLfloat xlowerLimit = -dia / 2.0f;
	GLfloat xupperLimit = dia / 2.0f;
	GLfloat zlowerLimit = -dia / 2.0f;
	GLfloat zupperLimit = dia / 2.0f;
	GLfloat x1=0.0f, x2=0.0f, xTemp=0.0f;
	/* Create a flat grid */
	k = 0;
	for (i = 0; i < MAP_NUM_VERTICES; ++i)
	{
		
		for (j = 0; j < MAP_NUM_VERTICES; ++j)
		{
			map_vertices[0][k] = x;
			map_vertices[1][k] = y;
			map_vertices[2][k] = z;

			map_textures[0][k] = (x + ((GLfloat)MAP_SIZE / 2.0f))/ (GLfloat)MAP_SIZE;
			map_textures[1][k] = (z + ((GLfloat)MAP_SIZE / 2.0f))/ (GLfloat)MAP_SIZE;
			
			z += step;
			++k;

			if (z >= zlowerLimit && z <= zupperLimit && x >= xlowerLimit && x <= xupperLimit)
			{
				x1 = fabs(xlowerLimit - x);
				x2 = fabs(xupperLimit - x);
				if (x1<=2)
				{
					xTemp = x1;
				}
				else {
					xTemp = x2;
				}
				y = elevationFactor * sinf(yAngle* 3.1415f/180.0f);
				y = y * fabs(dia - xTemp)/dia;
				yAngle += angleStep;
				fprintf(gpErrorLog, "Vertice (%f, %f, %f) %f\n", x, y, z, xTemp);

			}
			/*else if (x > -upperLimit && x < -lowerLimit && z > lowerLimit && z < upperLimit)
			{
				x1 = fabs(10.0f - fabs(x));
				x2 = fabs(40.0f - fabs(x));
				if (x1 < x2)
				{
					xTemp = x1;
				}
				else {
					xTemp = x2;
				}
				y = sinf(yAngle* 3.1415f / 180.0f);
				xTempAngle = xTemp * 90.0f / 15.0f;

				y = y * xTemp * 24.0f / MAP_SIZE;
				yAngle += angleStep;
			}*/
			else {
				y = 0.0f;
			}

		}
		x += step;
		z = -MAP_SIZE / 2.0f;
		y = 0.0f;
		yAngle = 0.0f;
	}

#if DEBUG_ENABLED
	for (i = 0; i < MAP_NUM_TOTAL_VERTICES; ++i)
	{
		fprintf(gpFile, "Vertice %d (%f, %f, %f)\n", i, map_vertices[0][i], map_vertices[1][i], map_vertices[2][i]);
		fprintf(gpFile, "Vertice %d (%f, %f)\n", i, map_textures[0][i], map_textures[1][i]);
	}
#endif
	/* create indices */
	/* line fan based on i
	 * i+1
	 * |  / i + n + 1
	 * | /
	 * |/
	 * i --- i + n
	 */

	 /* close the top of the square */
	k = 0;
	for (i = 0; i < MAP_NUM_VERTICES - 1; ++i)
	{
		map_line_indices[k++] = (i + 1) * MAP_NUM_VERTICES - 1;
		map_line_indices[k++] = (i + 2) * MAP_NUM_VERTICES - 1;
	}
	/* close the right of the square */
	for (i = 0; i < MAP_NUM_VERTICES - 1; ++i)
	{
		map_line_indices[k++] = (MAP_NUM_VERTICES - 1) * MAP_NUM_VERTICES + i;
		map_line_indices[k++] = (MAP_NUM_VERTICES - 1) * MAP_NUM_VERTICES + i + 1;
	}

	for (i = 0; i < (MAP_NUM_VERTICES - 1); ++i)
	{
		for (j = 0; j < (MAP_NUM_VERTICES - 1); ++j)
		{
			int ref = i * (MAP_NUM_VERTICES)+j;
			map_line_indices[k++] = ref;
			map_line_indices[k++] = ref + 1;

			map_line_indices[k++] = ref;
			map_line_indices[k++] = ref + MAP_NUM_VERTICES;

			map_line_indices[k++] = ref;
			map_line_indices[k++] = ref + MAP_NUM_VERTICES + 1;
		}
	}

#ifdef DEBUG_ENABLED
	for (k = 0; k < 2 * MAP_NUM_LINES; k += 2)
	{
		int beg, end;
		beg = map_line_indices[k];
		end = map_line_indices[k + 1];
		printf("Line %d: %d -> %d (%f, %f, %f) -> (%f, %f, %f)\n",
			k / 2, beg, end,
			map_vertices[0][beg], map_vertices[1][beg], map_vertices[2][beg],
			map_vertices[0][end], map_vertices[1][end], map_vertices[2][end]);
	}
#endif
}

static void generate_heightmap__circle(float* center_x, float* center_y,
	float* size, float* displacement)
{
	float sign;
	/* random value for element in between [0-1.0] */
	*center_x = (MAP_SIZE * rand()) / (1.0f * RAND_MAX);
	*center_y = (MAP_SIZE * rand()) / (1.0f * RAND_MAX);
	*size = (MAX_CIRCLE_SIZE * rand()) / (1.0f * RAND_MAX);
	sign = (1.0f * rand()) / (1.0f * RAND_MAX);
	sign = (sign < DISPLACEMENT_SIGN_LIMIT) ? -1.0f : 1.0f;
	*displacement = (sign * (MAX_DISPLACEMENT * rand())) / (1.0f * RAND_MAX);
}

/* Run the specified number of iterations of the generation process for the
 * heightmap
 */
static void update_map(int num_iter)
{
	assert(num_iter > 0);
	while (num_iter)
	{
		/* center of the circle */
		float center_x;
		float center_z;
		float circle_size;
		float disp;
		size_t ii;
		generate_heightmap__circle(&center_x, &center_z, &circle_size, &disp);
		disp = disp / 2.0f;
		for (ii = 0u; ii < MAP_NUM_TOTAL_VERTICES; ++ii)
		{
			GLfloat dx = center_x - map_vertices[0][ii];
			GLfloat dz = center_z - map_vertices[2][ii];
			GLfloat pd = (2.0f * (float)sqrt((dx * dx) + (dz * dz))) / circle_size;
			if (fabs(pd) <= 1.0f)
			{
				/* tx,tz is within the circle */
				GLfloat new_height = disp + (float)(cos(pd*3.14f)*disp);
				map_vertices[1][ii] += new_height;
			}
		}
		--num_iter;
	}
}


void uninitialize()
{
	if (bIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwstyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	//safe uninitialize
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "fclose Successful");
		fclose(gpFile);
		gpFile = NULL;
	}
	if (gpErrorLog)
	{
		fprintf(gpErrorLog, "fclose Successful");
		fclose(gpErrorLog);
		gpErrorLog = NULL;
	}

	//release programmable pipleline objects
	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);

	if (mesh_vbo)
	{
		glDeleteBuffers(1, mesh_vbo);
		//mesh_vbo = 0;
	}

	if (mesh_vao)
	{
		glDeleteVertexArrays(1, &mesh_vao);
	}
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen == false)
	{
		dwstyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwstyle & WS_OVERLAPPEDWINDOW) {
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, dwstyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		bIsFullScreen = true;
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwstyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bIsFullScreen = false;
	}
}


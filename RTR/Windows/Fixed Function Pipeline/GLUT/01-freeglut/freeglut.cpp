#include <GL/freeglut.h>
#include <math.h>
#include <windows.graphics.h>

bool bIsFullscreen = false;

//variables
GLfloat static R1 = 1.0f, G1 = 0.0f, B1 = 0.0f;
GLfloat static R2 = 0.0f, G2 = 1.0f, B2 = 0.0f;
GLfloat static R3 = 0.0f, G3 = 0.0f, B3 = 1.0f;

int static a1 = 0, a2 = 5, a3 = 10;

GLfloat static vx1 = 0.0f, vy1 = 0.5f, vx2 = -0.5f, vy2 = -0.5f, vx3 = 0.5f, vy3 = -0.5f;
bool static yPositive1=true, yPositive2=true, yPositive3=false;
bool static R1Positive = true, G1Positive = false, B1Positive = false;
bool static R2Positive = false, G2Positive = true, B2Positive = false;
bool static R3Positive = false, G3Positive = false, B3Positive = true;

bool static rotateTriangle = true;


//declaration
void rotate(void);
void colorChange(void);

int main(int argc, char *argv[])
{
	//fun declarations
	void initialize(void);
	void uninitialize(void);
	void reshape(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	//void keyboard2(unsigned char, int, int);
	void mouse(int, int, int, int);

	//code
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	glutInitWindowSize(1000, 800);
	glutInitWindowPosition(100, 100);

	glutCreateWindow("MyFirstOpenGLProgram");
	initialize();

	//callbacks
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	//glutKeyboardFunc(keyboard2);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);
	glutMainLoop();

}

void initialize(void) {
	//code first openGL fun calling
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void uninitialize(void) {

}

void reshape(int width, int height)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

}

void display(void)
{

	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glBegin(GL_TRIANGLES);

	glColor3f(R1, G1, B1);
	glVertex2f(vx1, vy1);

	glColor3f(R2, G2, B2);
	glVertex2f(vx2, vy2);

	glColor3f(R3, G3, B3);
	glVertex2f(vx3, vy3);

	glEnd();
	glFlush();
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'f':
	case 'F':
		if (bIsFullscreen == false)
		{
			glutFullScreen();
			bIsFullscreen = true;

		}
		else {
			glutLeaveFullScreen();
			bIsFullscreen = false;
		}
		break;
	case 'r':
	case 'R':
		while (rotateTriangle==true)
		{
			colorChange();
			rotate();
		}
		break;
	default:
		break;
	}
}

void keyboard2(unsigned char key, int x, int y) {
	switch (key)
	{
	case 'p':
	case 'P':
		if (rotateTriangle == true)
			rotateTriangle = false;
		else
			rotateTriangle = true;
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	}
}

void colorChange(void) {
	//color change
	a1 = (a1 + 1)%90;
	a2 = (a2 + 3)%90;
	a3 = (a3 + 5)%90;

	if (R1 >= 0.0f && R1Positive == true)
		R1 = R1 - sin(a1);
	else {
		R1Positive = false;
		R1 = R1 + sin(a1);
		if (R1 >= 1.0f)
			R1Positive = true;
	}

	if (G1 < 1.0f && G1Positive == false)
		G1 = G1 + sin(a1);
	else {
		G1Positive = true;
		G1 = G1 - sin(a1);
		if (G1 <= 0.0f)
			R1Positive = false;
	}

	if (B1 < 1.0f && B1Positive == false)
		B1 = B1 + sin(a1);
	else {
		B1Positive = true;
		B1 = B1 - sin(a1);
		if (B1 <= 0.0f)
			B1Positive = false;
	}
	if (R2 < 1.0f && R2Positive == false)
		R2 = R2 + sin(a2);
	else {
		R2Positive = true;
		R2 = R2 - sin(a2);
		if (R2 <= 0.0f)
			R2Positive = false;
	}

	if (G2 >= 0.0f && G2Positive == true)
		G2 = G2 - sin(a2);
	else {
		G2Positive = false;
		G2 = G2 + sin(a2);
		if (G2 >= 1.0f)
			G2Positive = true;
	}

	if (B2 < 1.0f && B2Positive == false)
		B2 = B2 + sin(a2);
	else {
		R2Positive = true;
		B2 = B2 - sin(a2);
		if (B2 <= 0.0f)
			B2Positive = false;
	}

	if (R3 < 1.0f && R3Positive == false)
		R3 = R3 + sin(a3);
	else {
		R3Positive = true;
		R3 = R3 - sin(a3);
		if (R3 <= 0.0f)
			R3Positive = false;
	}

	if (G3 < 1.0f && G3Positive == false)
		G3 = G3 + sin(a3);
	else {
		G3Positive = true;
		G3 = G3 - sin(a3);
		if (G3 <= 0.0f)
			G3Positive = false;
	}

	if (B3 >= 0.0f && B3Positive == true)
		B3 = B3 - sin(a3);
	else {
		B3Positive = false;
		B3 = B3 + sin(B3);
		if (B3 >= 1.0f)
			B3Positive = true;
	}

}
void rotate(void) {

	//move vertices
	if (vx1 <= 0.45f && (yPositive1 == true))
	{
		vx1 = vx1 + 0.05f;
	}
	else
	{
		vx1 = vx1 - 0.05f;
		yPositive1 = false;
		if (vx1 <= -0.5f)
		{
			yPositive1 = true;
		}
	}

	if (vx1 == 0.5f || vx1 == -0.5f)
	{
		vy1 = 0.0f;
	}
	else
	{
		if (yPositive1 == true)
			vy1 = (GLfloat)sqrt(0.25f - vx1 * vx1);
		else
			vy1 = -(GLfloat)sqrt(0.25f - vx1 * vx1);
	}

	//2nd vertex
	/*
	if ((vx2>=-0.45f && vx2 <= 0.45f) && (yPositive2 == false))
	{
		vx2 = vx2 - 0.05f;
	}
	*/


	if (vx2 <= 0.45f && (yPositive2 == true))
	{
		vx2 = vx2 + 0.05f;
	}
	else
	{
		vx2 = vx2 - 0.05f;
		yPositive2 = false;
		if (vx2 <= -0.5f)
		{
			yPositive2 = true;
		}
	}

	if (vx2 == 0.5f || vx2 == -0.5f)
	{
		vy2 = 0.0f;
	}
	else
	{
		if (yPositive2 == true)
			vy2 = (GLfloat)sqrt(0.5f - vx2 * vx2);
		else
			vy2 = -(GLfloat)sqrt(0.5f - vx2 * vx2);
	}

	//3rd vertex
	if (vx3 >= -0.45f && (yPositive3 == false))
	{
		vx3 = vx3 - 0.05f;
	}
	else
	{
		vx3 = vx3 + 0.05f;
		yPositive3 = true;
		if (vx3 >= 0.5f)
		{
			yPositive3 = false;
		}
	}

	if (vx3 == 0.5f || vx3 == -0.5f)
	{
		vy3 = 0.0f;
	}
	else
	{
		if (yPositive3 == true)
			vy3 = (GLfloat)sqrt(0.25f - vx3 * vx3);
		else
			vy3 = -(GLfloat)sqrt(0.25f - vx3 * vx3);
	}
	display();
	Sleep(100);
}


#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <stdio.h>
#include <math.h>
#include "Resource.h"
//#include <playsoundapi.h>

//for linker: link openGL library
#pragma comment(lib, "Opengl32.lib")
#pragma comment(lib, "glu32.lib")
#pragma comment(lib, "winmm.lib")


//constants
#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define PI 3.141592653589793

//Global vars
HDC ghdc = NULL;
HGLRC ghrc = NULL; //openGL rendoring context
bool gbActivWindow = false;
DWORD dwstyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;
FILE *gpFile = NULL;

//speed vars
GLfloat speed = (GLfloat) 0.0005f;
GLfloat planeSpeed = speed + (GLfloat) 0.0003f;

//circle variables
const int numpoints = 1000;

//parabola
GLfloat px, py;
GLfloat h = 0.0f, k = 0.0f;
GLfloat a = 0.009f;

//hyperbola
GLfloat hx, hy;
GLfloat hh = 0.0f, hk = 0.0f;
GLfloat ha = 0.1f;
GLfloat c = 0.15f;
GLfloat b = sqrtf(c * c - ha * ha);

//letters translate variables
GLfloat tz = -3.0f;
GLfloat txI = -1.0f;
GLfloat tyN = 1.0f;
GLfloat tzD = 0.0f;
GLfloat tyI = -1.0f;
GLfloat txA = 1.0f;

//Aeroplane rotate, translate vars
GLfloat aeroPlaneRotateAngle = 90.0f;
GLfloat txp = -4.0f, typ = 2.7f;

GLfloat width = 0.06f;
GLfloat gap = 0.08f;
GLfloat letterStretch = 0.3f;

void drawAeroPlane();

//global fun
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	int initialize();

	//fun declaration for double buffer window
	void display();

	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Dynamic India");
	bool bDone = false;
	int iRet;

	//code 
	//Log file creation
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log file is created successfully\n");
	}

	//initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; //CS_OWNDC: To tell OS that this DC is specialized DC, hence it should not be discarded
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	//register wndclass
	RegisterClassEx(&wndclass);



	//create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Dynamic India"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		75,
		75,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{

		fprintf(gpFile, "SetPixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "WglCreateContext() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "WglMakeCurrent() Failed \n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "initialize() function succeeded \n");
	}

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	//game loop
	while (bDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				bDone = true;
			}
			else {
				//msg loop
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			//play game
			if (gbActivWindow == true) {

			}
			display();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_CREATE:
		PlaySound(MAKEINTRESOURCE(SOUND_RESOURCE), ((LPCREATESTRUCT)lParam)->hInstance, SND_RESOURCE | SND_ASYNC);
		break;
	case WM_SETFOCUS:
		gbActivWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActivWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return 0;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_PAINT:
		ToggleFullScreen();
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;
	dwstyle = GetWindowLong(ghwnd, GWL_STYLE);
	if (dwstyle & WS_OVERLAPPEDWINDOW)
	{
		mi = { sizeof(MONITORINFO) };
		if (GetWindowPlacement(ghwnd, &wpPrev)
			&& GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
		{
			SetWindowLong(ghwnd, GWL_STYLE, dwstyle & ~WS_OVERLAPPEDWINDOW);
			SetWindowPos(ghwnd, HWND_TOP,
				mi.rcMonitor.left,
				mi.rcMonitor.top,
				mi.rcMonitor.right - mi.rcMonitor.left,
				mi.rcMonitor.bottom - mi.rcMonitor.top,
				SWP_NOZORDER | SWP_FRAMECHANGED);
		}
	}
	ShowCursor(FALSE);
}

int initialize(void)
{
	void resize(int, int);
	//variables
	PIXELFORMATDESCRIPTOR pfd;
	INT iPixelFormatIndex;

	//initialize 
	ZeroMemory((void *)&pfd, sizeof(PIXELFORMATDESCRIPTOR));		//initialize pfd structure and pfd members to 0
	//memset((void *)&pfd, NULL, size(PIXELFORMATDESCRIPTOR));		//another way

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0) {
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		return -2;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		return -4;
	}

	//Clear screen
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	//warm up call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);
	return 0;
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);

	//I
	GLfloat vx1 = -0.785f, vy1 = 0.25f;
	glLoadIdentity();
	glTranslatef(txI, 0.0f, tz);

	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vx1, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vx1, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vx1 + width, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vx1 + width, vy1);
	glEnd();


	//N
	GLfloat vxN = vx1 + width + gap;
	glLoadIdentity();
	glTranslatef(0.0f, tyN, tz);

	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxN, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxN, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxN + width, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxN + width, vy1);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxN, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxN + letterStretch, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxN + width + letterStretch, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxN + width, vy1);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxN + letterStretch, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxN + letterStretch, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxN + letterStretch + width, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxN + letterStretch + width, vy1);
	glEnd();


	//D
	GLfloat vxD = vxN + width + gap + letterStretch;
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, tzD);

	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxD, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxD, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxD + width, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxD + width, vy1);

	glEnd();

	GLfloat angle = (GLfloat)PI / 2.0f;

	glBegin(GL_QUADS);
	for (GLfloat i = 0.0f; i < 2.0*PI; i = i + 0.001f)
	{
		if (i <= PI)
		{
			for (int j = 0; j < 10; j++);
			{
				GLfloat move = 0.0f;
				glColor3f(1.0f, 0.6f, 0.2f);
				glVertex3f(vxD + width + (GLfloat)cos(angle)*vy1 - move, (GLfloat)sin(angle)*vy1 - move, 0.0f);
				move = move + 0.01f;
			}
			angle = angle - 0.001f;
		}
		else
		{
			if (i <= 2.0f*PI)
			{
				for (int k = 0; k < 10; k++)
				{
					GLfloat move = 0.0f;
					glColor3f(0.0742f, 0.5333f, 0.3137f);
					glVertex3f(vxD + width + (GLfloat)cos(angle)*(vy1)+move, (GLfloat)sin(angle)*(vy1)+move, 0.0f);
					move = move + 0.01f;
				}
			}
			glColor3f(1.0f, 0.6f, 0.2f);
			glVertex3f(vxD + width + (GLfloat)cos(angle)*(vy1 - width), (GLfloat)sin(angle)*(vy1 - width), 0.0f);
			angle = angle + 0.001f;
		}
	}
	glEnd();

	//I
	GLfloat vxI = vxD + width + gap + vy1;
	glLoadIdentity();
	glTranslatef(0.0f, tyI, tz);

	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxI, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxI, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxI + width, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxI + width, vy1);
	glEnd();

	//A
	GLfloat vxA = vxI + width + gap;
	letterStretch = 0.2f;
	glLoadIdentity();
	glTranslatef(txA, 0.0f, tz);

	glBegin(GL_QUADS);

	if (txp > (vxA + 1.5f))
	{
		//orange
		glVertex2f(vxA + width + letterStretch / 2.0f, 0.035f);
		glColor3f(1.0f, 0.6f, 0.2f);
		glVertex2f(vxA + width + letterStretch / 2.0f, 0.0055f);
		glColor3f(1.0f, 0.6f, 0.2f);
		glVertex2f(vxA + letterStretch + letterStretch / 2.0f, 0.0055f);
		glColor3f(1.0f, 0.6f, 0.2f);
		glVertex2f(vxA + letterStretch + letterStretch / 2.0f, 0.035f);

		//white
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex2f(vxA + width + letterStretch / 2.0f - 0.01f, 0.0055f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex2f(vxA + width + letterStretch / 2.0f - 0.01f, -0.024f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex2f(vxA + letterStretch + letterStretch / 2.0f + 0.01f, -0.024f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex2f(vxA + letterStretch + letterStretch / 2.0f + 0.01f, 0.0055f);

		//green
		glColor3f(0.0742f, 0.5333f, 0.3137f);
		glVertex2f(vxA + width + letterStretch / 2.0f - 0.02f, -0.024f);
		glColor3f(0.0742f, 0.5333f, 0.3137f);
		glVertex2f(vxA + width + letterStretch / 2.0f - 0.02f, -0.0535f);
		glColor3f(0.0742f, 0.5333f, 0.3137f);
		glVertex2f(vxA + letterStretch + letterStretch / 2.0f + 0.02f, -0.0535f);
		glColor3f(0.0742f, 0.5333f, 0.3137f);
		glVertex2f(vxA + letterStretch + letterStretch / 2.0f + 0.02f, -0.024f);
	}

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxA + letterStretch, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxA, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxA + width, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxA + width + letterStretch, vy1);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxA + letterStretch, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxA + letterStretch * 2.0f, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxA + letterStretch * 2.0f + width, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxA + width + letterStretch, vy1);

	glEnd();

	//circle
	angle = 0.0f;
	glTranslatef(vxA + (width / 2.0f) + letterStretch, -0.00925f, 0.0f);
	glBegin(GL_POINTS);
	for (GLfloat i = 0.0f; i < 2.0*PI; i = i + 0.1f)
	{
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f((GLfloat)cos(angle)*0.012f, (GLfloat)sin(angle)*0.012f, 0.0f);
		angle = angle + 0.1f;
	}
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.012f, 0.0f, 0.0f);
	glVertex3f(0.012f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.012f, 0.0f);
	glVertex3f(0.0f, -0.012f, 0.0f);
	glVertex3f((GLfloat)cos(3.0f*PI / 4.0f)*0.012f, (GLfloat)sin(3.0f*PI / 4.0f)*0.012f, 0.0f);
	glVertex3f((GLfloat)cos(7.0f*PI / 4.0f)*0.012f, (GLfloat)sin(7.0f*PI / 4.0f)*0.012f, 0.0f);
	glVertex3f((GLfloat)cos(PI / 4.0f)*0.012f, (GLfloat)sin(PI / 4.0f)*0.012f, 0.0f);
	glVertex3f((GLfloat)cos(5.0f*PI / 4.0f)*0.012f, (GLfloat)sin(5.0f*PI / 4.0f)*0.012f, 0.0f);
	glEnd();

	//plane from top
	glLoadIdentity();
	glRotatef(aeroPlaneRotateAngle, 0.0f, 0.0f, 1.0f);
	glTranslatef(txp, typ, -5.0f);
	drawAeroPlane();

	//plane from middle
	glLoadIdentity();
	glTranslatef(txp, 0.0f, -5.0f);
	drawAeroPlane();

	//plane from bottum
	glLoadIdentity();
	glRotatef(-aeroPlaneRotateAngle, 0.0f, 0.0f, 1.0f);
	glTranslatef(txp, -typ, -5.0f);
	drawAeroPlane();

	SwapBuffers(ghdc);

	//I x
	if (txI <= 0.0f) {
		txI = txI + speed;
	}

	//N y
	if (tyN >= 0.0f) {
		tyN = tyN - speed;
	}

	//D y
	if (tzD >= -3.0f) {
		tzD = tzD - speed * 3.0f;
	}

	//I y
	if (tyI <= 0.0f) {
		tyI = tyI + speed;
	}

	//I x
	if (txA >= 0.0f) {
		txA = txA - speed;
	}

	//Aeroplanes translate
	if (txI >= 0.0f && txp<=8.0f)
	{
		txp = txp + planeSpeed;

		if (txp < (vxA + 1.8f))
		{
			if (typ >= 0.0f)
			{
				typ = typ - planeSpeed;

			}
			if (aeroPlaneRotateAngle >= 0.0f)
			{
				aeroPlaneRotateAngle = aeroPlaneRotateAngle - 0.0266667f;
			}
		}
		else
		{
			if (aeroPlaneRotateAngle<=30.0f)
			{
				aeroPlaneRotateAngle = aeroPlaneRotateAngle + 0.0105f;
			}
			if (typ<=2.7f)
			{
				typ = typ + planeSpeed - 0.0008f;
			}
		}
		fprintf(gpFile, "txp: %f\n", txp);
	}
	letterStretch = 0.3f;
}

void drawAeroPlane() {
	//plane front cockpit: parabola
	GLfloat px, py;
	glBegin(GL_POLYGON);
	for (py = 0.1f; py >= -0.101f; py = py - 0.001f)
	{
		px = (GLfloat)(h - (py - k)*(py - k) / (4 * a));
		glColor3f(0.729f, 0.886f, 0.933f);
		glVertex2f(px, py);
	}

	//tail- hyperbola
	hh = px - 0.6f, hk = 0.0f;
	for (GLfloat hy = -0.101f; hy <= 0.1f; hy = hy + 0.001f)
	{
		hx = (GLfloat)((sqrtf((ha*ha)*(1 + (hy - hk)*(hy - hk) / (b*b)))) + hh);
		glColor3f(0.729f, 0.886f, 0.933f);
		glVertex2f(hx, hy);
	}
	glEnd();

	//exhaust
	glBegin(GL_POINTS);
	hh = px - 0.6f, hk = 0.0f;
	for (GLfloat hy = -0.101f; hy <= 0.1f; hy = hy + 0.005f)
	{
		hx = (GLfloat)((sqrtf((ha*ha)*(1 + (hy - hk)*(hy - hk) / (b*b)))) + hh);
		for (GLfloat i = 0.0f; i < 0.3f; i= i + 0.01f)
		{
			if (hy>=0.0333f)
			{
				glColor3f(1.0f, 0.6f, 0.2f);
				glVertex2f(hx - i, hy);
			}
			else if (hy>=-0.0333f)
			{
				glColor3f(1.0f, 1.0f, 1.0f);
				glVertex2f(hx - i, hy);
			}
			else if (hy > -0.1)
			{
				glColor3f(0.0742f, 0.5333f, 0.3137f);
				glVertex2f(hx - i, hy);
			}
		}
	}
	glEnd();

	//plane wings
	GLfloat wingsWidth = 0.2f;
	GLfloat wingsHeight = 0.3001f;
	GLfloat wx = px - 0.3f;
	GLfloat wy = py + 0.5f;

	glBegin(GL_TRIANGLES);
	glColor3f(0.729f, 0.886f, 0.933f);
	glVertex2f(wx, wy);
	glVertex2f(wx, wy - wingsHeight);
	glVertex2f(wx + wingsWidth, wy - wingsHeight);

	wy = py;
	glColor3f(0.729f, 0.886f, 0.933f);
	glVertex2f(wx, wy + 0.1f);
	glVertex2f(wx, wy - wingsHeight);
	glVertex2f(wx + wingsWidth, wy);
	glEnd();

	glBegin(GL_QUADS);
	GLfloat wWidth = 0.05f;
	wy = py + 0.5f;

	glColor3f(0.729f, 0.886f, 0.933f);
	glVertex2f(wx, wy);
	glVertex2f(wx - wWidth, wy);
	glVertex2f(wx - wWidth, wy - wingsHeight);
	glVertex2f(wx, wy - wingsHeight);

	wy = py;
	glColor3f(0.729f, 0.886f, 0.933f);
	glVertex2f(wx, wy);
	glVertex2f(wx - wWidth, wy);
	glVertex2f(wx - wWidth, wy - wingsHeight);
	glVertex2f(wx, wy - wingsHeight);
	glEnd();
}

void uninitialize(void) {
	SetWindowLong(ghwnd, GWL_STYLE, dwstyle | WS_OVERLAPPEDWINDOW);
	SetWindowPlacement(ghwnd, &wpPrev);
	SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_FRAMECHANGED | SWP_NOSIZE | SWP_NOOWNERZORDER);
	ShowCursor(TRUE);

	//safe uninitialize
	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "File is closed");
		fclose(gpFile);
		gpFile = NULL;
	}
}

#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <stdio.h>
#include <math.h>

//for linker: link openGL library
#pragma comment(lib, "Opengl32.lib")
#pragma comment(lib, "glu32.lib")

//constants
#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define PI 3.141592653589793

//Global vars
HDC ghdc = NULL;
HGLRC ghrc = NULL; //openGL rendoring context
bool gbActivWindow = false;
bool bIsFullScreen = false;
DWORD dwstyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;
FILE *gpFile = NULL;

//circle variables
const int numpoints = 1000;

//co-ordinate variables
GLfloat vx1 = -0.785f, vy1 = 0.25f;
GLfloat width = 0.06f;
GLfloat gap = 0.08f;
GLfloat letterStretch = 0.3f;

static GLfloat rotateAngle = 0.0f;
static GLfloat speed = 0.02f;
static bool rotateGeometry = false;

//global fun
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	int initialize();

	//fun declaration for double buffer window
	void display();
	void rotate();

	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("GLUT triangle");
	bool bDone = false;
	int iRet;

	//code 
	//Log file creation
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log file is created successfully\n");
	}

	//initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; //CS_OWNDC: To tell OS that this DC is specialized DC, hence it should not be discarded
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	//register wndclass
	RegisterClassEx(&wndclass);

	//create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("InCircle"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		75,
		75,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{

		fprintf(gpFile, "SetPixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "WglCreateContext() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "WglMakeCurrent() Failed \n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "initialize() function succeeded \n");
	}

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	//game loop
	while (bDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				bDone = true;
			}
			else {
				//msg loop
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			//play game
			if (gbActivWindow == true) {

			}
			if (rotateGeometry)
			{
				rotate();
			}
			display();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActivWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActivWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return 0;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
			ToggleFullScreen();
			break;
		case 'r':
		case 'R':
			rotateGeometry = true;
			break;
		case 'd':
		case 'D':
			speed = speed + 0.03f;
			break;
		case 's':
		case 'S':
			speed = speed - 0.03f;
			break;
		}
		break;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void rotate()
{
	rotateAngle = rotateAngle + speed;
	if (rotateAngle >= 360.0f)
	{
		rotateAngle = 0.0f;
	}
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen == false)
	{
		dwstyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwstyle & WS_OVERLAPPEDWINDOW) {
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, dwstyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		bIsFullScreen = true;
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwstyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bIsFullScreen = false;
	}
}

int initialize(void)
{
	void resize(int, int);
	//variables
	PIXELFORMATDESCRIPTOR pfd;
	INT iPixelFormatIndex;

	//initialize 
	ZeroMemory((void *)&pfd, sizeof(PIXELFORMATDESCRIPTOR));		//initialize pfd structure and pfd members to 0
	//memset((void *)&pfd, NULL, size(PIXELFORMATDESCRIPTOR));		//another way

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0) {
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE) {
		return -2;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		return -4;
	}

	//Clear screen
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	//warm up call to resize
	resize(WIN_WIDTH, WIN_HEIGHT);
	return 0;
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -2.0f);

	//I
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vx1, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vx1, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vx1 + width, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vx1 + width, vy1);
	glEnd();	

	//N
	GLfloat vxN = vx1 + width + gap;
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxN, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxN, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxN+width, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxN+width, vy1);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxN, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxN + letterStretch, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxN+width + letterStretch, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxN + width, vy1);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxN + letterStretch, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxN + letterStretch, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxN + letterStretch + width, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxN + letterStretch + width, vy1);

	//D
	GLfloat vxD = vxN + width + gap + letterStretch;
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxD, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxD, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxD + width, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxD + width, vy1);

	glEnd();

	GLfloat angle = (GLfloat)PI/2.0f;
	
	glBegin(GL_QUADS);
	for (GLfloat i = 0.0f; i < 2.0*PI; i = i + 0.001f)
	{
		if (i<=PI)
		{
			for (int j = 0; j <10; j++);
			{
				GLfloat move = 0.0f;
				glColor3f(1.0f, 0.6f, 0.2f);
				glVertex3f(vxD + width + cos(angle)*vy1 - move, sin(angle)*vy1 - move, 0.0f);
				move = move + 0.01f;
			}
			angle = angle - 0.001f;
		}
		else
		{
			if (i<=2.0f*PI)
			{
				for (int k = 0; k < 10; k++)
				{
					GLfloat move = 0.0f;
					glColor3f(0.0742f, 0.5333f, 0.3137f);
					glVertex3f(vxD + width + cos(angle)*(vy1) + move, sin(angle)*(vy1) + move, 0.0f);
					move = move + 0.01f;
				}
			}
			glColor3f(1.0f, 0.6f, 0.2f);
			glVertex3f(vxD + width + cos(angle)*(vy1-width), sin(angle)*(vy1- width), 0.0f);
			angle = angle + 0.001f;
		}
	}
	glEnd();

	//I
	GLfloat vxI = vxD + width + gap + vy1;
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxI, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxI, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxI + width, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxI + width, vy1);
	glEnd();

	//A
	GLfloat vxA = vxI+ width + gap;
	letterStretch = 0.2f;

	glBegin(GL_QUADS);
	//orange
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxA + width + letterStretch / 2.0f, 0.035f);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxA + width + letterStretch / 2.0f, 0.0055f);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxA + letterStretch + letterStretch / 2.0f, 0.0055f);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxA + letterStretch + letterStretch / 2.0f, 0.035f);

	//white
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex2f(vxA + width + letterStretch / 2.0f - 0.01f, 0.0055f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex2f(vxA + width + letterStretch / 2.0f - 0.01f, -0.024f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex2f(vxA + letterStretch + letterStretch / 2.0f + 0.01f, -0.024f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex2f(vxA + letterStretch + letterStretch / 2.0f + 0.01f, 0.0055f);

	//green
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxA + width + letterStretch / 2.0f - 0.02f, -0.024f);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxA + width + letterStretch / 2.0f - 0.02f, -0.0535f);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxA + letterStretch + letterStretch / 2.0f + 0.02f, -0.0535f);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxA + letterStretch + letterStretch / 2.0f + 0.02f, -0.024f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxA+ letterStretch, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxA, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxA + width, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxA + width+ letterStretch, vy1);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxA + letterStretch, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxA + letterStretch*2.0f, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxA + letterStretch*2.0f + width, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxA + width + letterStretch, vy1);
	glEnd();

	//circle
	angle = 0.0f;
	glTranslatef(vxA + (width / 2.0f) + letterStretch, -0.00925f, 0.0f);
	glBegin(GL_POINTS);
	for (GLfloat i = 0.0f; i < 2.0*PI; i = i + 0.1f)
	{
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(cos(angle)*0.012f, sin(angle)*0.012f, 0.0f);
		angle = angle + 0.1f;
	}
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.012f, 0.0f, 0.0f);
	glVertex3f(0.012f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.012f, 0.0f);
	glVertex3f(0.0f, -0.012f, 0.0f);
	glVertex3f(cos(3.0f*PI/4.0f)*0.012f, sin(3.0f*PI / 4.0f)*0.012f, 0.0f);
	glVertex3f(cos(7.0f*PI / 4.0f)*0.012f, sin(7.0f*PI / 4.0f)*0.012f, 0.0f);
	glVertex3f(cos(PI / 4.0f)*0.012f, sin(PI / 4.0f)*0.012f, 0.0f);
	glVertex3f(cos(5.0f*PI / 4.0f)*0.012f, sin(5.0f*PI / 4.0f)*0.012f, 0.0f);
	glEnd();

	SwapBuffers(ghdc);
	letterStretch = 0.3f;

}

void uninitialize(void) {
	if (bIsFullScreen == true) {
		SetWindowLong(ghwnd, GWL_STYLE, dwstyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOMOVE | SWP_FRAMECHANGED | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	//safe uninitialize
	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile) {
		fprintf(gpFile, "File is closed");
		fclose(gpFile);
		gpFile = NULL;
	}
}

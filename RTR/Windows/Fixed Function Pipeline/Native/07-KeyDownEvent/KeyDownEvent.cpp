#include <windows.h>

// Global Function Declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// Variable Declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	// Code
	// Initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// Register above Class
	RegisterClassEx(&wndclass);

	// Create Window
	hwnd = CreateWindow(szAppName,
		TEXT("My Application"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	// Message Loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Code
	switch (iMsg)
	{
	case WM_KEYDOWN:
		switch (wParam)
		{
		case 'a':
		case 'A':
			MessageBox(hwnd, TEXT("A key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'b':
		case 'B':
			MessageBox(hwnd, TEXT("B key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'c':
		case 'C':
			MessageBox(hwnd, TEXT("C key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'd':
		case 'D':
			MessageBox(hwnd, TEXT("D key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'e':
		case 'E':
			MessageBox(hwnd, TEXT("E key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'f':
		case 'F':
			MessageBox(hwnd, TEXT("F key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'G':
		case 'g':
			MessageBox(hwnd, TEXT("G key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'h':
		case 'H':
			MessageBox(hwnd, TEXT("H key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'i':
		case 'I':
			MessageBox(hwnd, TEXT("I key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'j':
		case 'J':
			MessageBox(hwnd, TEXT("J key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'k':
		case 'K':
			MessageBox(hwnd, TEXT("K key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'l':
		case 'L':
			MessageBox(hwnd, TEXT("L key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'm':
		case 'M':
			MessageBox(hwnd, TEXT("M key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'n':
		case 'N':
			MessageBox(hwnd, TEXT("N key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'o':
		case 'O':
			MessageBox(hwnd, TEXT("O key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'p':
		case 'P':
			MessageBox(hwnd, TEXT("P key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'q':
		case 'Q':
			MessageBox(hwnd, TEXT("Q key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'r':
		case 'R':
			MessageBox(hwnd, TEXT("R key is pressed"), TEXT("message"), MB_OK);
			break;
		case 's':
		case 'S':
			MessageBox(hwnd, TEXT("S key is pressed"), TEXT("message"), MB_OK);
			break;
		case 't':
		case 'T':
			MessageBox(hwnd, TEXT("T key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'u':
		case 'U':
			MessageBox(hwnd, TEXT("U key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'v':
		case 'V':
			MessageBox(hwnd, TEXT("V key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'w':
		case 'W':
			MessageBox(hwnd, TEXT("W key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'x':
		case 'X':
			MessageBox(hwnd, TEXT("X key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'y':
		case 'Y':
			MessageBox(hwnd, TEXT("Y key is pressed"), TEXT("message"), MB_OK);
			break;
		case 'z':
		case 'Z':
			MessageBox(hwnd, TEXT("Z key is pressed"), TEXT("message"), MB_OK);
			break;
		case '1':
			MessageBox(hwnd, TEXT("1 key is pressed"), TEXT("message"), MB_OK);
			break;
		case '2':
			MessageBox(hwnd, TEXT("2 key is pressed"), TEXT("message"), MB_OK);
			break;
		case '3':
			MessageBox(hwnd, TEXT("3 key is pressed"), TEXT("message"), MB_OK);
			break;
		case '4':
			MessageBox(hwnd, TEXT("4 key is pressed"), TEXT("message"), MB_OK);
			break;
		case '5':
			MessageBox(hwnd, TEXT("5 key is pressed"), TEXT("message"), MB_OK);
			break; 
		case '6':
			MessageBox(hwnd, TEXT("6 key is pressed"), TEXT("message"), MB_OK);
			break;
		case '7':
			MessageBox(hwnd, TEXT("7 key is pressed"), TEXT("message"), MB_OK);
			break;
		case '8':
			MessageBox(hwnd, TEXT("8 key is pressed"), TEXT("message"), MB_OK);
			break;
		case '9':
			MessageBox(hwnd, TEXT("9 key is pressed"), TEXT("message"), MB_OK);
			break;
		case '0':
			MessageBox(hwnd, TEXT("0 key is pressed"), TEXT("message"), MB_OK);
			break;
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

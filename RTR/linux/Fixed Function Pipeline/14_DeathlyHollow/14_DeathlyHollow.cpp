#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

//header gl
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

//namespaces
using namespace std;

//global variables
bool bFullScreen = False;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
GLXContext gglXContext = NULL;
int giWindowWidth = 800;
int giWindowHeight = 600;

bool gbActivWindow = false;
bool bIsFullScreen = false;
FILE *gpFile = NULL;

//circle variables
const int numpoints = 1000;
const GLfloat PI = 3.1415;

//triangle vars
GLfloat vx1 = 0.0f, vy1 = 0.5f;
GLfloat vx2 = -0.5f, vy2 = -0.5f;
GLfloat vx3 = 0.5f, vy3 = -0.5f;
GLfloat lenght1, lenght2, lenght3;

//entry point function
int main(void)
{
	//fun prototypes
	void CreateWindow(void);
	void initialize(void);
	void ToggleFullScreen(void);
	void uninitialize();
	void resize(int, int);
	void display(void);

	//vars
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	static XFontStruct *pXFontStruct=NULL;
	static GC gc;
	XGCValues gcValues;
	XColor text_color;
	char str[255] = "DeathlyHollow";
	int strLenght;
	int strWidth;
	int fontHeight;

	Bool bDone = False;

	//code
	CreateWindow();

	//message loop
	initialize();
	XEvent event;

	KeySym keysym;

	while (bDone==False)
	{
		
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case MapNotify:
					pXFontStruct = XLoadQueryFont(gpDisplay, "fixed");
					break;
				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch (keysym)
					{
						case XK_Escape:
							bDone = True;
							break;
						case XK_F:
						case XK_f:
							if (bFullScreen == False)
							{
								ToggleFullScreen();
								bFullScreen = True;
							}
							else {
								ToggleFullScreen();
								bFullScreen = False;
							}
							break;
						default:
							break;
					}
					break;
				case ButtonPress:
					switch (event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						case 4:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
	       			winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone=True;
					break;
				default:
					break;
			}
	
		}
		//update();		
		display();
	}
	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	///function prototypes
	void uninitialize();
	void resize(int, int);

	//variable declaration
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	

	static int frameBufferAttribute[]=
		{GLX_RGBA,
		GLX_DOUBLEBUFFER, True,
		GLX_RED_SIZE,1,
		GLX_GREEN_SIZE,1,
		GLX_BLUE_SIZE,1,
		GLX_ALPHA_SIZE, 1,
		GLX_DEPTH_SIZE, 24,
		None};


	//code
	gpDisplay = XOpenDisplay(NULL);
	if (gpDisplay == NULL)
	{
		printf("Error: unable to open X display. \nExiting Now... \n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttribute);
	if (gpXVisualInfo == NULL)
	{
		printf("Erro: Unable to allocate memory for visual info\n Exiting\n");
		uninitialize();
		exit(1);
	}
	
	
	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		gpXVisualInfo->visual,
		AllocNone);

	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);

	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,
		0,
		giWindowWidth,
		giWindowHeight,
		0,
		gpXVisualInfo->depth,
		InputOutput,
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs
	);

	if (!gWindow)
	{
		printf("Error: Failed to create main window. \n Exiting now. \n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "DeathlyHollow");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(gpDisplay, gWindow);

}


void initialize(void)
{
	//function
	void resize(int, int);
	void uninitialize(void);
	
	gglXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

	//to get hardware context
	glXMakeCurrent(gpDisplay, gWindow, gglXContext);
	 
	//usual opengl code
	
	//Clear screen
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	//warm up call to resize
	resize(giWindowWidth, giWindowWidth);
}


void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f
	);
}


void display(void)
{
	
	glClear(GL_COLOR_BUFFER_BIT);
	
	//below code is same in openGL and xWindows

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
	glLineWidth(2.0f);
	//Triangle
	glBegin(GL_LINE_LOOP);
	glVertex2f(vx1, vy1);
	glVertex2f(vx2, vy2);
	glVertex2f(vx3, vy3);
	glEnd();

	lenght1 = sqrt((vx2 - vx1)*(vx2 - vx1) + (vy2 - vy1)*(vy2 - vy1));
	lenght2 = sqrt((vx3 - vx2)*(vx3 - vx2) + (vy3 - vy2)*(vy3 - vy2));
	lenght3 = sqrt((vx1 - vx3)*(vx1 - vx3) + (vy1 - vy3)*(vy1 - vy3));

	if (lenght1 == lenght2)
	{
		printf("1-2 sides same\n");
	}
	if (lenght2 == lenght3)
	{
		printf("2-3 sides same\n");
	}
	if (lenght1 == lenght3)
	{
		printf("1-3 sides same\n");
	}

	//locate center
	GLfloat cx = ((lenght1 * vx1) + (lenght2 * vx2) + (lenght3 * vx3)) / (lenght1 + lenght2 + lenght3);
	GLfloat cy = ((lenght1 * vy1) + (lenght2 * vy2) + (lenght3 * vy3)) / (lenght1 + lenght2 + lenght3);
	
	//radius
	GLfloat s = (lenght1 + lenght2 + lenght3) / 2;
	GLfloat Radius = (sqrt(s * (s - lenght1) * (s - lenght2) * (s - lenght3)))/s;
	//Translate
	glTranslatef(cx-0.02f, cy-0.03f, 0.0f);

	//draw circle
	GLfloat angle, x, y;

	glBegin(GL_LINE_LOOP);
	for (int point = 0; point <= numpoints; point++)
	{
		angle = (2.0f * PI) * ((GLfloat(point) / GLfloat(numpoints)));
		x = Radius * cos(angle);
		y = Radius * sin(angle);
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	//vertical line
	glBegin(GL_LINES);
	glVertex2f(vx1, vy1+0.188f);
	glVertex2f(vx1, vy1-0.82f);
	glEnd();

	
	//swap buffers
	glXSwapBuffers(gpDisplay, gWindow);

}


void ToggleFullScreen(void)
{
	//vars
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = { 0 };

	//code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False,
		StructureNotifyMask,
		&xev);

}



void uninitialize(void)
{
	if (gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	if (gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);

	}

	if (gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	if (gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}

	GLXContext currentglXContext = glXGetCurrentContext();

	if (currentglXContext != NULL && currentglXContext == gglXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);

	}
	
	if (gglXContext)
	{
		glXDestroyContext(gpDisplay, gglXContext);	
		gglXContext = NULL;
	}

	//usual code from openGL
	
	//as memory might get allocated inside glxChooseVisual
	if (gpXVisualInfo)
	{
		XFree(gpXVisualInfo);
	}

}

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <math.h>
#include "Resource.h"

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>
#include <X11/Xresource.h>

//header gl
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

//namespaces
using namespace std;
#define PI 3.141592653589793

//global variables
bool bFullScreen = False;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
GLXContext gglXContext = NULL;
int giWindowWidth = 800;
int giWindowHeight = 600;

bool gbActivWindow = false;
bool bIsFullScreen = false;
FILE *gpFile = NULL;


//speed vars
GLfloat speed = (GLfloat) 0.0005f;
GLfloat planeSpeed = speed + (GLfloat) 0.0003f;

//circle variables
const int numpoints = 1000;

//parabola
GLfloat px, py;
GLfloat h = 0.0f, k = 0.0f;
GLfloat a = 0.009f;

//hyperbola
GLfloat hx, hy;
GLfloat hh = 0.0f, hk = 0.0f;
GLfloat ha = 0.1f;
GLfloat c = 0.15f;
GLfloat b = sqrtf(c * c - ha * ha);

//letters translate variables
GLfloat tz = -3.0f;
GLfloat txI = -1.0f;
GLfloat tyN = 1.0f;
GLfloat tzD = 0.0f;
GLfloat tyI = -1.0f;
GLfloat txA = 1.0f;

//Aeroplane rotate, translate vars
GLfloat aeroPlaneRotateAngle = 90.0f;
GLfloat txp = -4.0f, typ = 2.7f;

GLfloat width = 0.06f;
GLfloat gap = 0.08f;
GLfloat letterStretch = 0.3f;

void drawAeroPlane(void);

//entry point function
int main(void)
{
	//fun prototypes
	void CreateWindow(void);
	void initialize(void);
	void ToggleFullScreen(void);
	void uninitialize();
	void resize(int, int);
	void display(void);

	//vars
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	static XFontStruct *pXFontStruct=NULL;
	static GC gc;
	XGCValues gcValues;
	XColor text_color;
	char str[255] = "DynamicIndia";
	int strLenght;
	int strWidth;
	int fontHeight;

	Bool bDone = False;

	//code
	CreateWindow();

	//message loop
	initialize();
	XEvent event;

	KeySym keysym;

	while (bDone==False)
	{
		
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case CreateNotify:
					//PlaySound(MAKEINTRESOURCE(SOUND_RESOURCE), ((LPCREATESTRUCT)lParam)->hInstance, SND_RESOURCE | SND_ASYNC);
					break;
				case MapNotify:
					pXFontStruct = XLoadQueryFont(gpDisplay, "fixed");
					break;
				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch (keysym)
					{
						case XK_Escape:
							bDone = True;
							break;
						case XK_F:
						case XK_f:
							if (bFullScreen == False)
							{
								ToggleFullScreen();
								bFullScreen = True;
							}
							else {
								ToggleFullScreen();
								bFullScreen = False;
							}
							break;
						default:
							break;
					}
					break;
				case ButtonPress:
					switch (event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						case 4:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
	       			winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone=True;
					break;
				default:
					break;
			}
	
		}
		//update();		
		display();
	}
	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	///function prototypes
	void uninitialize();
	void resize(int, int);

	//variable declaration
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	

	static int frameBufferAttribute[]=
		{GLX_RGBA,
		GLX_DOUBLEBUFFER, True,
		GLX_RED_SIZE,1,
		GLX_GREEN_SIZE,1,
		GLX_BLUE_SIZE,1,
		GLX_ALPHA_SIZE, 1,
		GLX_DEPTH_SIZE, 24,
		None};


	//code
	gpDisplay = XOpenDisplay(NULL);
	if (gpDisplay == NULL)
	{
		printf("Error: unable to open X display. \nExiting Now... \n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttribute);
	if (gpXVisualInfo == NULL)
	{
		printf("Erro: Unable to allocate memory for visual info\n Exiting\n");
		uninitialize();
		exit(1);
	}
	
	
	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		gpXVisualInfo->visual,
		AllocNone);

	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);

	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,
		0,
		giWindowWidth,
		giWindowHeight,
		0,
		gpXVisualInfo->depth,
		InputOutput,
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs
	);

	if (!gWindow)
	{
		printf("Error: Failed to create main window. \n Exiting now. \n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "DynamicIndia");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(gpDisplay, gWindow);

}


void initialize(void)
{
	//function
	void resize(int, int);
	void uninitialize(void);
	
	gglXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

	//to get hardware context
	glXMakeCurrent(gpDisplay, gWindow, gglXContext);
	 
	//usual opengl code
	
	//Clear screen
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	//warm up call to resize
	resize(giWindowWidth, giWindowWidth);
}


void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f
	);
}


void display(void)
{
	
	glClear(GL_COLOR_BUFFER_BIT);
	
	//below code is same in openGL and xWindows

	glMatrixMode(GL_MODELVIEW);

	//I
	GLfloat vx1 = -0.785f, vy1 = 0.25f;
	glLoadIdentity();
	glTranslatef(txI, 0.0f, tz);

	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vx1, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vx1, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vx1 + width, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vx1 + width, vy1);
	glEnd();


	//N
	GLfloat vxN = vx1 + width + gap;
	glLoadIdentity();
	glTranslatef(0.0f, tyN, tz);

	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxN, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxN, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxN + width, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxN + width, vy1);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxN, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxN + letterStretch, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxN + width + letterStretch, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxN + width, vy1);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxN + letterStretch, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxN + letterStretch, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxN + letterStretch + width, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxN + letterStretch + width, vy1);
	glEnd();


	//D
	GLfloat vxD = vxN + width + gap + letterStretch;
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, tzD);

	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxD, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxD, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxD + width, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxD + width, vy1);

	glEnd();

	GLfloat angle = (GLfloat)PI / 2.0f;

	glBegin(GL_QUADS);
	for (GLfloat i = 0.0f; i < 2.0*PI; i = i + 0.001f)
	{
		if (i <= PI)
		{
			for (int j = 0; j < 10; j++);
			{
				GLfloat move = 0.0f;
				glColor3f(1.0f, 0.6f, 0.2f);
				glVertex3f(vxD + width + (GLfloat)cos(angle)*vy1 - move, (GLfloat)sin(angle)*vy1 - move, 0.0f);
				move = move + 0.01f;
			}
			angle = angle - 0.001f;
		}
		else
		{
			if (i <= 2.0f*PI)
			{
				for (int k = 0; k < 10; k++)
				{
					GLfloat move = 0.0f;
					glColor3f(0.0742f, 0.5333f, 0.3137f);
					glVertex3f(vxD + width + (GLfloat)cos(angle)*(vy1)+move, (GLfloat)sin(angle)*(vy1)+move, 0.0f);
					move = move + 0.01f;
				}
			}
			glColor3f(1.0f, 0.6f, 0.2f);
			glVertex3f(vxD + width + (GLfloat)cos(angle)*(vy1 - width), (GLfloat)sin(angle)*(vy1 - width), 0.0f);
			angle = angle + 0.001f;
		}
	}
	glEnd();

	//I
	GLfloat vxI = vxD + width + gap + vy1;
	glLoadIdentity();
	glTranslatef(0.0f, tyI, tz);

	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxI, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxI, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxI + width, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxI + width, vy1);
	glEnd();

	//A
	GLfloat vxA = vxI + width + gap;
	letterStretch = 0.2f;
	glLoadIdentity();
	glTranslatef(txA, 0.0f, tz);

	glBegin(GL_QUADS);

	if (txp > (vxA + 1.5f))
	{
		//orange
		glVertex2f(vxA + width + letterStretch / 2.0f, 0.035f);
		glColor3f(1.0f, 0.6f, 0.2f);
		glVertex2f(vxA + width + letterStretch / 2.0f, 0.0055f);
		glColor3f(1.0f, 0.6f, 0.2f);
		glVertex2f(vxA + letterStretch + letterStretch / 2.0f, 0.0055f);
		glColor3f(1.0f, 0.6f, 0.2f);
		glVertex2f(vxA + letterStretch + letterStretch / 2.0f, 0.035f);

		//white
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex2f(vxA + width + letterStretch / 2.0f - 0.01f, 0.0055f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex2f(vxA + width + letterStretch / 2.0f - 0.01f, -0.024f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex2f(vxA + letterStretch + letterStretch / 2.0f + 0.01f, -0.024f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex2f(vxA + letterStretch + letterStretch / 2.0f + 0.01f, 0.0055f);

		//green
		glColor3f(0.0742f, 0.5333f, 0.3137f);
		glVertex2f(vxA + width + letterStretch / 2.0f - 0.02f, -0.024f);
		glColor3f(0.0742f, 0.5333f, 0.3137f);
		glVertex2f(vxA + width + letterStretch / 2.0f - 0.02f, -0.0535f);
		glColor3f(0.0742f, 0.5333f, 0.3137f);
		glVertex2f(vxA + letterStretch + letterStretch / 2.0f + 0.02f, -0.0535f);
		glColor3f(0.0742f, 0.5333f, 0.3137f);
		glVertex2f(vxA + letterStretch + letterStretch / 2.0f + 0.02f, -0.024f);
	}

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxA + letterStretch, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxA, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxA + width, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxA + width + letterStretch, vy1);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxA + letterStretch, vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxA + letterStretch * 2.0f, -vy1);
	glColor3f(0.0742f, 0.5333f, 0.3137f);
	glVertex2f(vxA + letterStretch * 2.0f + width, -vy1);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(vxA + width + letterStretch, vy1);

	glEnd();

	//circle
	angle = 0.0f;
	glTranslatef(vxA + (width / 2.0f) + letterStretch, -0.00925f, 0.0f);
	glBegin(GL_POINTS);
	for (GLfloat i = 0.0f; i < 2.0*PI; i = i + 0.1f)
	{
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f((GLfloat)cos(angle)*0.012f, (GLfloat)sin(angle)*0.012f, 0.0f);
		angle = angle + 0.1f;
	}
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(-0.012f, 0.0f, 0.0f);
	glVertex3f(0.012f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.012f, 0.0f);
	glVertex3f(0.0f, -0.012f, 0.0f);
	glVertex3f((GLfloat)cos(3.0f*PI / 4.0f)*0.012f, (GLfloat)sin(3.0f*PI / 4.0f)*0.012f, 0.0f);
	glVertex3f((GLfloat)cos(7.0f*PI / 4.0f)*0.012f, (GLfloat)sin(7.0f*PI / 4.0f)*0.012f, 0.0f);
	glVertex3f((GLfloat)cos(PI / 4.0f)*0.012f, (GLfloat)sin(PI / 4.0f)*0.012f, 0.0f);
	glVertex3f((GLfloat)cos(5.0f*PI / 4.0f)*0.012f, (GLfloat)sin(5.0f*PI / 4.0f)*0.012f, 0.0f);
	glEnd();

	//plane from top
	glLoadIdentity();
	glRotatef(aeroPlaneRotateAngle, 0.0f, 0.0f, 1.0f);
	glTranslatef(txp, typ, -5.0f);
	drawAeroPlane();

	//plane from middle
	glLoadIdentity();
	glTranslatef(txp, 0.0f, -5.0f);
	drawAeroPlane();

	//plane from bottum
	glLoadIdentity();
	glRotatef(-aeroPlaneRotateAngle, 0.0f, 0.0f, 1.0f);
	glTranslatef(txp, -typ, -5.0f);
	drawAeroPlane();

	
	//swap buffers
	glXSwapBuffers(gpDisplay, gWindow);

//I x
	if (txI <= 0.0f) {
		txI = txI + speed;
	}

	//N y
	if (tyN >= 0.0f) {
		tyN = tyN - speed;
	}

	//D y
	if (tzD >= -3.0f) {
		tzD = tzD - speed * 3.0f;
	}

	//I y
	if (tyI <= 0.0f) {
		tyI = tyI + speed;
	}

	//I x
	if (txA >= 0.0f) {
		txA = txA - speed;
	}

	//Aeroplanes translate
	if (txI >= 0.0f && txp<=8.0f)
	{
		txp = txp + planeSpeed;

		if (txp < (vxA + 1.8f))
		{
			if (typ >= 0.0f)
			{
				typ = typ - planeSpeed;

			}
			if (aeroPlaneRotateAngle >= 0.0f)
			{
				aeroPlaneRotateAngle = aeroPlaneRotateAngle - 0.0266667f;
			}
		}
		else
		{
			if (aeroPlaneRotateAngle<=30.0f)
			{
				aeroPlaneRotateAngle = aeroPlaneRotateAngle + 0.0105f;
			}
			if (typ<=2.7f)
			{
				typ = typ + planeSpeed - 0.0008f;
			}
		}
		//fprintf(gpFile, "txp: %f\n", txp);
	}
	letterStretch = 0.3f;

}

void drawAeroPlane(void) {
	//plane front cockpit: parabola
	GLfloat px, py;
	glBegin(GL_POLYGON);
	for (py = 0.1f; py >= -0.101f; py = py - 0.001f)
	{
		px = (GLfloat)(h - (py - k)*(py - k) / (4 * a));
		glColor3f(0.729f, 0.886f, 0.933f);
		glVertex2f(px, py);
	}

	//tail- hyperbola
	hh = px - 0.6f, hk = 0.0f;
	for (GLfloat hy = -0.101f; hy <= 0.1f; hy = hy + 0.001f)
	{
		hx = (GLfloat)((sqrtf((ha*ha)*(1 + (hy - hk)*(hy - hk) / (b*b)))) + hh);
		glColor3f(0.729f, 0.886f, 0.933f);
		glVertex2f(hx, hy);
	}
	glEnd();

	//exhaust
	glBegin(GL_POINTS);
	hh = px - 0.6f, hk = 0.0f;
	for (GLfloat hy = -0.101f; hy <= 0.1f; hy = hy + 0.005f)
	{
		hx = (GLfloat)((sqrtf((ha*ha)*(1 + (hy - hk)*(hy - hk) / (b*b)))) + hh);
		for (GLfloat i = 0.0f; i < 0.3f; i= i + 0.01f)
		{
			if (hy>=0.0333f)
			{
				glColor3f(1.0f, 0.6f, 0.2f);
				glVertex2f(hx - i, hy);
			}
			else if (hy>=-0.0333f)
			{
				glColor3f(1.0f, 1.0f, 1.0f);
				glVertex2f(hx - i, hy);
			}
			else if (hy > -0.1)
			{
				glColor3f(0.0742f, 0.5333f, 0.3137f);
				glVertex2f(hx - i, hy);
			}
		}
	}
	glEnd();

	//plane wings
	GLfloat wingsWidth = 0.2f;
	GLfloat wingsHeight = 0.3001f;
	GLfloat wx = px - 0.3f;
	GLfloat wy = py + 0.5f;

	glBegin(GL_TRIANGLES);
	glColor3f(0.729f, 0.886f, 0.933f);
	glVertex2f(wx, wy);
	glVertex2f(wx, wy - wingsHeight);
	glVertex2f(wx + wingsWidth, wy - wingsHeight);

	wy = py;
	glColor3f(0.729f, 0.886f, 0.933f);
	glVertex2f(wx, wy + 0.1f);
	glVertex2f(wx, wy - wingsHeight);
	glVertex2f(wx + wingsWidth, wy);
	glEnd();

	glBegin(GL_QUADS);
	GLfloat wWidth = 0.05f;
	wy = py + 0.5f;

	glColor3f(0.729f, 0.886f, 0.933f);
	glVertex2f(wx, wy);
	glVertex2f(wx - wWidth, wy);
	glVertex2f(wx - wWidth, wy - wingsHeight);
	glVertex2f(wx, wy - wingsHeight);

	wy = py;
	glColor3f(0.729f, 0.886f, 0.933f);
	glVertex2f(wx, wy);
	glVertex2f(wx - wWidth, wy);
	glVertex2f(wx - wWidth, wy - wingsHeight);
	glVertex2f(wx, wy - wingsHeight);
	glEnd();
}

void ToggleFullScreen(void)
{
	//vars
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = { 0 };

	//code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False,
		StructureNotifyMask,
		&xev);

}



void uninitialize(void)
{
	if (gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	if (gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);

	}

	if (gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	GLXContext currentglXContext = glXGetCurrentContext();

	if (currentglXContext != NULL && currentglXContext == gglXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);

	}
	
	if (gglXContext)
	{
		glXDestroyContext(gpDisplay, gglXContext);	
		gglXContext = NULL;
	}

	if (gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
	//usual code from openGL
	
	//as memory might get allocated inside glxChooseVisual

}

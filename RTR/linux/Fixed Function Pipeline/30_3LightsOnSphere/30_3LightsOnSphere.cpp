#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

//header gl
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

//namespaces
using namespace std;

//global variables
bool bFullScreen = False;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
GLXContext gglXContext = NULL;
int giWindowWidth = 800;
int giWindowHeight = 600;

bool gbActivWindow = false;
bool bIsFullScreen = false;
FILE *gpFile = NULL;

GLfloat angle = 0.0f;
GLUquadric* Quadric = NULL;
bool bLight = false;

GLfloat angleZeroLight = 0.0f;
GLfloat angleOneLight = 0.0f;
GLfloat angleTwoLight = 0.0f;

struct Light
{
	GLfloat Ambient[4];
	GLfloat Diffuse[4];
	GLfloat Specular[4];
	GLfloat Position[4];
}light[2];



GLfloat AmbientMaterial[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat DiffuseMaterial[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat SpecularMaterial[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat ShininessMaterial[] = { 128.0f };


//entry point function
int main(void)
{
	//fun prototypes
	void CreateWindow(void);
	void initialize(void);
	void ToggleFullScreen(void);
	void uninitialize();
	void resize(int, int);
	void display(void);

	//vars
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	static XFontStruct *pXFontStruct=NULL;
	static GC gc;
	XGCValues gcValues;
	XColor text_color;
	char str[255] = "3LightsOnSphere";
	int strLenght;
	int strWidth;
	int fontHeight;

	Bool bDone = False;

	//code
	CreateWindow();

	//message loop
	initialize();
	XEvent event;

	KeySym keysym;

	while (bDone==False)
	{
		
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case MapNotify:
					pXFontStruct = XLoadQueryFont(gpDisplay, "fixed");
					break;
				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch (keysym)
					{
						case XK_Escape:
							bDone = True;
							break;
						case XK_F:
						case XK_f:
							if (bFullScreen == False)
							{
								ToggleFullScreen();
								bFullScreen = True;
							}
							else {
								ToggleFullScreen();
								bFullScreen = False;
							}
							break;
						default:
							break;
					}
					break;
				case ButtonPress:
					switch (event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						case 4:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
	       			winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone=True;
					break;
				default:
					break;
			}
	
		}
		//update();		
		display();
	}
	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	///function prototypes
	void uninitialize();
	void resize(int, int);

	//variable declaration
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	

	static int frameBufferAttribute[]=
		{GLX_RGBA,
		GLX_DOUBLEBUFFER, True,
		GLX_RED_SIZE,1,
		GLX_GREEN_SIZE,1,
		GLX_BLUE_SIZE,1,
		GLX_ALPHA_SIZE, 1,
		GLX_DEPTH_SIZE, 24,
		None};


	//code
	gpDisplay = XOpenDisplay(NULL);
	if (gpDisplay == NULL)
	{
		printf("Error: unable to open X display. \nExiting Now... \n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttribute);
	if (gpXVisualInfo == NULL)
	{
		printf("Erro: Unable to allocate memory for visual info\n Exiting\n");
		uninitialize();
		exit(1);
	}
	
	
	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		gpXVisualInfo->visual,
		AllocNone);

	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);

	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,
		0,
		giWindowWidth,
		giWindowHeight,
		0,
		gpXVisualInfo->depth,
		InputOutput,
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs
	);

	if (!gWindow)
	{
		printf("Error: Failed to create main window. \n Exiting now. \n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "3LightsOnSphere");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(gpDisplay, gWindow);

}


void initialize(void)
{
	//function
	void resize(int, int);
	void uninitialize(void);

	//Light1
	light[0].Ambient[0] = 0.0f;
	light[0].Ambient[1] = 0.0f;
	light[0].Ambient[2] = 0.0f;
	light[0].Ambient[3] = 0.0f;

	light[0].Diffuse[0] = 1.0f;
	light[0].Diffuse[1] = 0.0f;
	light[0].Diffuse[2] = 0.0f;
	light[0].Diffuse[3] = 0.0f;

	light[0].Specular[0] = 1.0f;
	light[0].Specular[1] = 0.0f;
	light[0].Specular[2] = 0.0f;
	light[0].Specular[3] = 0.0f;

	light[0].Position[0] = 0.0f;
	light[0].Position[1] = 0.0f;
	light[0].Position[2] = 0.0f;
	light[0].Position[3] = 0.0f;

	//Light2
	light[1].Ambient[0] = 0.0f;
	light[1].Ambient[1] = 0.0f;
	light[1].Ambient[2] = 0.0f;
	light[1].Ambient[3] = 0.0f;

	light[1].Diffuse[0] = 0.0f;
	light[1].Diffuse[1] = 1.0f;
	light[1].Diffuse[2] = 0.0f;
	light[1].Diffuse[3] = 0.0f;

	light[1].Specular[0] = 0.0f;
	light[1].Specular[1] = 1.0f;
	light[1].Specular[2] = 0.0f;
	light[1].Specular[3] = 0.0f;

	light[1].Position[0] = 0.0f;
	light[1].Position[1] = 0.0f;
	light[1].Position[2] = 0.0f;
	light[1].Position[3] = 0.0f;

	//Light3
	light[2].Ambient[0] = 0.0f;
	light[2].Ambient[1] = 0.0f;
	light[2].Ambient[2] = 0.0f;
	light[2].Ambient[3] = 0.0f;

	light[2].Diffuse[0] = 0.0f;
	light[2].Diffuse[1] = 0.0f;
	light[2].Diffuse[2] = 1.0f;
	light[2].Diffuse[3] = 0.0f;

	light[2].Specular[0] = 0.0f;
	light[2].Specular[1] = 0.0f;
	light[2].Specular[2] = 1.0f;
	light[2].Specular[3] = 0.0f;

	light[2].Position[0] = 0.0f;
	light[2].Position[1] = 0.0f;
	light[2].Position[2] = 0.0f;
	light[2].Position[3] = 0.0f;
	
	gglXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

	//to get hardware context
	glXMakeCurrent(gpDisplay, gWindow, gglXContext);
	 
	//usual opengl code
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	//Clear screen
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//give existance to depth buffer
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);		//adding depth test
	glDepthFunc(GL_LEQUAL);			//test			
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_LIGHTING);

	glLightfv(GL_LIGHT0, GL_AMBIENT, light[0].Ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light[0].Diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light[0].Specular);
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT1, GL_AMBIENT, light[1].Ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light[1].Diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light[1].Specular);
	glEnable(GL_LIGHT1);
	glLightfv(GL_LIGHT2, GL_AMBIENT, light[2].Ambient);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, light[2].Diffuse);
	glLightfv(GL_LIGHT2, GL_SPECULAR, light[2].Specular);
	glEnable(GL_LIGHT2);
	glMaterialfv(GL_FRONT, GL_AMBIENT, AmbientMaterial);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, DiffuseMaterial);
	glMaterialfv(GL_FRONT, GL_SPECULAR, SpecularMaterial);
	glMaterialfv(GL_FRONT, GL_SHININESS, ShininessMaterial);
	
	//warm up call to resize
	resize(giWindowWidth, giWindowWidth);
}


void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f
	);
}


void display(void)
{
	//below code is same in openGL and xWindows

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		//making depth functional
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();
	gluLookAt(0.0f, 0.0f, 3.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	//Light 1
	glPushMatrix();
	glRotatef(angleZeroLight, 1.0f, 0.0f, 0.0f);
	light[0].Position[1] = angleZeroLight;
	glLightfv(GL_LIGHT0, GL_POSITION, light[0].Position);

	glPopMatrix();

	//Light 2
	glPushMatrix();

	glRotatef(angleOneLight, 0.0f, 1.0f, 0.0f);
	light[1].Position[2] = angleOneLight;
	glLightfv(GL_LIGHT1, GL_POSITION, light[1].Position);

	glPopMatrix();

	//Light 3
	glPushMatrix();

	glRotatef(angleTwoLight, 0.0f, 0.0f, 1.0f);
	light[2].Position[0] = angleTwoLight;
	glLightfv(GL_LIGHT2, GL_POSITION, light[2].Position);

	glPopMatrix();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	Quadric = gluNewQuadric();
	gluSphere(Quadric, 0.7, 100, 100);
	glPopMatrix();
	
	//swap buffers
	glXSwapBuffers(gpDisplay, gWindow);

	if (angleZeroLight >= 360.0f)
	{
		angleZeroLight = 0.0f;
	}
	else
	{
		angleZeroLight = angleZeroLight + 0.1f;
	}

	if (angleOneLight >= 360.0f)
	{
		angleOneLight = 0.0f;
	}
	else
	{
		angleOneLight = angleOneLight + 0.1f;
	}

	if (angleTwoLight >= 360.0f)
	{
		angleTwoLight = 0.0f;
	}
	else
	{
		angleTwoLight = angleTwoLight + 0.1f;
	}

}


void ToggleFullScreen(void)
{
	//vars
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = { 0 };

	//code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False,
		StructureNotifyMask,
		&xev);

}



void uninitialize(void)
{
	if (gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	if (gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);

	}

	if (gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	GLXContext currentglXContext = glXGetCurrentContext();

	if (currentglXContext != NULL && currentglXContext == gglXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);

	}
	
	if (gglXContext)
	{
		glXDestroyContext(gpDisplay, gglXContext);	
		gglXContext = NULL;
	}

	if (gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
	//usual code from openGL
	
	//as memory might get allocated inside glxChooseVisual

}

package com.androidrtr.bluescreen;

import android.content.Context;
import android.content.SyncStatusObserver;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

//openGL
import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;    //basic openGL ES features
import javax.microedition.khronos.egl.EGLConfig;    //egl: embeded graphics library


public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{

    private final Context context;
    private GestureDetector gestureDetector;

    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context = drawingContext;

        setEGLContextClientVersion(3);  //informing EGL that we want version 3.x
        setRenderer(this);              //because we create the display function which itself does the rendering
        //onDrawFrame is anologous with display()

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);     //repaint me when the client area is dirty

        gestureDetector = new GestureDetector(drawingContext, this, null, false); // it means "handler" , who is going to handle
        gestureDetector.setOnDoubleTapListener(this); // this means 'handler', who is going to handle
    }

    // 'onTouchEvent' its imp
    // It Triggers All Gesture And Tap Events
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        if(!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);
        return(true);
    }

    //abstract method from OnDoubleTapListener
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        return(true);
    }

    //abstract method from OnDoubleTapListener
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        return(true);
    }

    //abstract method from OnDoubleTapListener
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        return(true);
    }

    //abstract method from OnGestureListener
    @Override
    public boolean onDown(MotionEvent e)
    {
        return(true);
    }

    //abstract method from OnGestureListener
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }

    //abstract method from OnGestureListener
    @Override
    public void onLongPress(MotionEvent e)
    {

    }

    //abstract method from OnGestureListener
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
		System.exit(0);
        return(true);
    }

    //abstract method from OnGestureListener
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    //abstract method from OnGestureListener
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }

    //implement GLSurfaceView.Renderer methods- 3 methods

    @Override
    public void onSurfaceCreated (GL10 gl, EGLConfig config)
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("RTR version: "+ version);
        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
    }

    //our custom methods
    private void initialize()
    {
        GLES32.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
    }

    private void resize(int width, int height)
    {
        GLES32.glViewport(0, 0, width, height);
    }

    private void display()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);

        //swap buffer
        requestRender();
    }

}

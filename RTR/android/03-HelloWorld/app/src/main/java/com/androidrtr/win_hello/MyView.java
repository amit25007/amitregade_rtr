package com.androidrtr.win_hello;

import androidx.appcompat.widget.AppCompatTextView;

import android.content.Context;
import android.view.GestureDetector;
import android.view.Gravity;
import android.widget.TextView;
import android.graphics.Color;

public class MyView extends AppCompatTextView {

    public MyView(Context drawingContext)
    {
        super(drawingContext);

        setTextColor(Color.rgb(255,128,0));
        setTextSize(60);
        setGravity(Gravity.CENTER);
        setText("Hello World !!!");
    }
}

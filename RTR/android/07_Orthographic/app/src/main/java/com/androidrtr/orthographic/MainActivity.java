package com.androidrtr.orthographic;

//import default given packages by android
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

//added by user
import android.view.Window;
import android.view.WindowManager;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import com.androidrtr.orthographic.GLESView;

public class MainActivity extends AppCompatActivity {

    private GLESView glesView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //to get rid of the titlebar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //to make full screen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        //force activity window orientation to landscape
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);

        //set backgrounf color
        this.getWindow().getDecorView().setBackgroundColor(Color.BLACK);


        //define out own view
        glesView = new GLESView(this);

        //set this as view as our view
        setContentView(glesView);

    }

    @Override
    protected void onPause()
    {
        super.onPause();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

}
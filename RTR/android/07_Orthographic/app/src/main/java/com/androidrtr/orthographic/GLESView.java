package com.androidrtr.orthographic;

import android.content.Context;
import android.content.SyncStatusObserver;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

//openGL
import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;    //basic openGL ES features
import javax.microedition.khronos.egl.EGLConfig;    //egl: embeded graphics library

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{

    private final Context context;
    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;    //java dont have uint nor gluint

    private int[] vao = new int[1]; //array because we cant pass address in java
    private int[] vbo = new int[1];
    private int mvpUniform;

    //declare ortho matrix
    private float[] orthographicProjectionMatrix = new float[16];       //4*4 matrix

    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context = drawingContext;

        setEGLContextClientVersion(3);  //informing EGL that we want version 3.x
        setRenderer(this);              //because we create the display function which itself does the rendering
        //onDrawFrame is anologous with display()

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);     //repaint me when the client area is dirty

        gestureDetector = new GestureDetector(drawingContext, this, null, false); // it means "handler" , who is going to handle
        gestureDetector.setOnDoubleTapListener(this); // this means 'handler', who is going to handle
    }

    // 'onTouchEvent' its imp
    // It Triggers All Gesture And Tap Events
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        if(!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);
        return(true);
    }

    //abstract method from OnDoubleTapListener
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        return(true);
    }

    //abstract method from OnDoubleTapListener
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        return(true);
    }

    //abstract method from OnDoubleTapListener
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        return(true);
    }

    //abstract method from OnGestureListener
    @Override
    public boolean onDown(MotionEvent e)
    {
        return(true);
    }

    //abstract method from OnGestureListener
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }

    //abstract method from OnGestureListener
    @Override
    public void onLongPress(MotionEvent e)
    {

    }

    //abstract method from OnGestureListener
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        uninitialize();
		System.exit(0);
        return(true);
    }

    //abstract method from OnGestureListener
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    //abstract method from OnGestureListener
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }

    //implement GLSurfaceView.Renderer methods- 3 methods

    @Override
    public void onSurfaceCreated (GL10 gl, EGLConfig config)
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("vdg");
        System.out.println("vdg RTR version: "+ version);
        String languageVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("vdg RTR language version: "+ languageVersion);
        String rendererVersion = gl.glGetString(GLES32.GL_RENDERER);            //opengl implementation name or driver name
        System.out.println("vdg RTR renderer version: "+ rendererVersion);
        String vendorVersion = gl.glGetString(GLES32.GL_VENDOR);              //company name
        System.out.println("vdg RTR vendor version: "+ vendorVersion);
        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
    }

    //our custom methods
    private void initialize()
    {
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
        final String vertexShaderSourceCode = String.format
                (
                "#version 320 es" +
                "\n" +
                "in vec4 vPosition;" +
                "uniform mat4 u_mvp_matrix;" +
                "void main(void)" +
                "{" +
                "gl_Position = u_mvp_matrix*vPosition;" +
                "}"
        );

        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
        GLES32.glCompileShader(vertexShaderObject);

        //Error checking
        int[] iShaderCompileStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
        if (iShaderCompileStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

            if (iInfoLogLength[0]>0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("vdg vertex shader compilation error: "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        //write fragment shader source code based on above code
        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        final String fragmentShaderSourceCode = String.format(
                "#version 320 es" +
                "\n" +
                "precision highp float;" +
                "out vec4 fragColor;" +
                "void main(void)" +
                "{" +
                "fragColor = vec4(1.0, 1.0, 1.0, 1.0);" +
                "}"
        );

        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
        GLES32.glCompileShader(fragmentShaderObject);

        //Error checking

        iShaderCompileStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;


        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
        if (iShaderCompileStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);

            if (iInfoLogLength[0]>0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("vdg fragment shader compilation error: "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        //Create and bind shader program object
        shaderProgramObject = GLES32.glCreateProgram();
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

        //prelink binding two attributes
        GLESMacros glesMacros = new GLESMacros();
        GLES32.glBindAttribLocation(shaderProgramObject, glesMacros.AMC_ATTRIBUTE_POSITION, "vPosition");

        //link
        GLES32.glLinkProgram(shaderProgramObject);

        //error check
        //Error checking for linking
        int[] iProgramLinkStatus = new int[1];
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);

        if (iProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if (iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(shaderProgramObject);
                System.out.println("vdg shader program link error: "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");

        final float[] triangleVertices = new float[]
                {
                        0.0f, 50.0f, 0.0f,
                        -50.0f, -50.0f, 0.0f,
                        50.0f, -50.0f, 0.0f
                };

        //create vao
        GLES32.glGenVertexArrays(1, vao, 0);
        GLES32.glBindVertexArray(vao[0]);

        //Create and bind vbo
        GLES32.glGenBuffers(1, vbo, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo[0]);


        // 1. Allocate buffer directly from native memory
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(triangleVertices.length*4); //4: size of float in java

        //2. Arrange the buffer to native byte order
        byteBuffer.order(ByteOrder.nativeOrder());

        //cretae the float buffer & convert our ByteBuffer into float type
        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();

        //put your array into cooked buffer
        positionBuffer.put(triangleVertices);

        //set the array at the 0th position of the buffer
        positionBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                triangleVertices.length*4,
                positionBuffer,
                GLES32.GL_STATIC_DRAW
                );

        GLES32.glVertexAttribPointer(glesMacros.AMC_ATTRIBUTE_POSITION,
                3,
                GLES32.GL_FLOAT,
                false,
                0,
                0       //in java null is not zero, in SDK #define NULL 0
                );

        GLES32.glEnableVertexAttribArray(glesMacros.AMC_ATTRIBUTE_POSITION);

        //bind buffer
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glBindVertexArray(0);

        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);
        GLES32.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

        Matrix.setIdentityM(orthographicProjectionMatrix, 0);

    }

    private void resize(int width, int height)
    {
        GLES32.glViewport(0, 0, width, height);
        Matrix.orthoM(orthographicProjectionMatrix,0,-100.0f * (float)(height / width), 100.0f * (float)(height / width), -100.0f, 100.0f, -100.0f, 100.0f);
    }

    private void display()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject);
        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];

        //Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        //Do necessary matrix multiplication
        Matrix.multiplyMM(modelViewProjectionMatrix, 0, orthographicProjectionMatrix, 0, modelViewMatrix, 0);
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        //bind vao
        GLES32.glBindVertexArray(vao[0]);
        //Similarly bind with textures if any

        //Draw necessary scene
        GLES32.glDrawArrays(GLES32.GL_TRIANGLES,
                0,		//from which position array will start
                3);		//

        //unbind vao
        GLES32.glBindVertexArray(0);

        //Unuse program
        GLES32.glUseProgram(0);

        //swap buffer
        requestRender();
    }

    private void uninitialize()
    {
//release programmable pipleline objects
        GLES32.glUseProgram(shaderProgramObject);
        GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
        GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);

        GLES32.glDeleteShader(fragmentShaderObject);
        fragmentShaderObject = 0;
        GLES32.glDeleteShader(vertexShaderObject);
        vertexShaderObject = 0;

        GLES32.glDeleteProgram(shaderProgramObject);
        shaderProgramObject = 0;
        GLES32.glUseProgram(0);

        if (vbo[0]!=0)
        {
            GLES32.glDeleteBuffers(1, vbo, 0);
            vbo[0] = 0;
        }

        if (vao[0]!=0)
        {
            GLES32.glDeleteVertexArrays(1, vao, 0);
        }
    }

}

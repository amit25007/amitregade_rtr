package com.androidrtr.gesture;

import androidx.appcompat.widget.AppCompatTextView;
import android.content.Context;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.Gravity;


public class MyView extends AppCompatTextView implements OnGestureListener, OnDoubleTapListener {

    private GestureDetector gestureDetector;

    public MyView(Context drawingContext)
    {
        super(drawingContext);

        setTextColor(Color.rgb(255,128,0));
        setTextSize(60);
        setGravity(Gravity.CENTER);
        setText("Hello World !!!");

        gestureDetector = new GestureDetector(drawingContext, this, null, false); // it means "handler" , who is going to handle
        gestureDetector.setOnDoubleTapListener(this); // this means 'handler', who is going to handle
    }

    // 'onTouchEvent' its imp
    // It Triggers All Gesture And Tap Events
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        if(!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);
        return(true);
    }

    //abstract method from OnDoubleTapListener
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        setText("Double Tap");
        return(true);
    }

    //abstract method from OnDoubleTapListener
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        return(true);
    }

    //abstract method from OnDoubleTapListener
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        setText("Single Tap");
        return(true);
    }

    //abstract method from OnGestureListener
    @Override
    public boolean onDown(MotionEvent e)
    {
        return(true);
    }

    //abstract method from OnGestureListener
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }

    //abstract method from OnGestureListener
    @Override
    public void onLongPress(MotionEvent e)
    {
        setText("Long Press");
    }

    //abstract method from OnGestureListener
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        setText("Scroll");
        return(true);
    }

    //abstract method from OnGestureListener
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    //abstract method from OnGestureListener
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }

}
